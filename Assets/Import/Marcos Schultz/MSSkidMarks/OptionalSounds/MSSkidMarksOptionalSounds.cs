﻿using UnityEngine;
using System.Collections;
using System;

[Serializable]
public class OtherSkiddingSoundsClass_ {
	[Tooltip("The sound that the vehicle will emit when skidding or skidding on a terrain defined by the tag of this index.")]
	public AudioClip skiddingSound;
	[Range(0.1f,3.0f)][Tooltip("The sound volume associated with the variable 'skiddingSound'")]
	public float volume = 1.5f;
	[Tooltip("The tag that will represent this index. Lands with this tag will receive skidlines based on the preferences of this index.")]
	public string groundTag;
	[Tooltip("The 'physicsMaterial' that will represent this index. Lands with this tag will receive sounds based on the preferences of this index.")]
	public PhysicMaterial physicMaterial;

	[Space(15)][Tooltip("The sound that the wheels will emit when they find a terrain defined by the tag or physicMaterial of this index.")]
	public AudioClip groundSound;
	[Range(0.01f,1.0f)][Tooltip("The sound volume associated with the variable 'groundSound'.")]
	public float volumeGroundSound = 0.15f;
}

[RequireComponent(typeof(Rigidbody))][RequireComponent(typeof(MSSkidMarks))]
public class MSSkidMarksOptionalSounds : MonoBehaviour {

	[Tooltip("The default sound that will be emitted when the vehicle slides or skates.")]
	public AudioClip standardSound;
	[Range(0.1f,3.0f)][Tooltip("The default volume of the skid sound.")]
	public float standardVolume = 1.5f;

	public enum GroundSelect {useTag, usePhysicsMaterial};
	[Space(20)][Tooltip("Here you can define whether the code will detect the ground by tag or through 'physics material'")]
	public GroundSelect groundDetection = GroundSelect.useTag;
	[Tooltip("The trail of skidding that the vehicle will do on land defined by tags should be set here.")]
	public OtherSkiddingSoundsClass_[] otherSounds;


	MSSkidMarks baseScript;
	AudioSource skiddingSoundAUD;
	AudioSource[] groundSoundsAUD;
	AudioSource[] groundSoundsAUDSkid;

	struct WheelListBase{
		public WheelCollider _wheelCollider;
		public bool _isSkidding;
		public bool useCustomSens;
		public float customSens;
	}
	WheelListBase[] wheelColliderListBase;

	int[] wheelEmitterSoundX;
	int[] wheelBlockSoundX;
	int[] wheelEmitterSoundXSkid;
	int[] wheelBlockSoundXSkid;

	WheelHit tempWheelHit;

	bool skiddingIsTrue;

	float KMhSounds;

	int maxWheels;
	int wheelsInOtherGround;
	float maxSlipTemp;
	float sidewaysSlipMaxSkid;
	float forwardSlipMaxSkid;
	float compareSidewaysSkid;
	float compareForwardSkid;


	void Start () {
		baseScript = GetComponent<MSSkidMarks> ();
		GenerateSKidSounds ();
		//
		wheelColliderListBase = new WheelListBase[(4 + baseScript.otherWheels.Length)];
		wheelColliderListBase[0]._wheelCollider = baseScript.rightFrontWheel.wheelCollider;
		wheelColliderListBase [0].useCustomSens = baseScript.rightFrontWheel.useCustomSensibility;
		wheelColliderListBase [0].customSens = baseScript.rightFrontWheel.customSensibility;
		wheelColliderListBase[1]._wheelCollider = baseScript.leftFrontWheel.wheelCollider;
		wheelColliderListBase [1].useCustomSens = baseScript.leftFrontWheel.useCustomSensibility;
		wheelColliderListBase [1].customSens = baseScript.leftFrontWheel.customSensibility;
		wheelColliderListBase[2]._wheelCollider = baseScript.rightRearWheel.wheelCollider;
		wheelColliderListBase [2].useCustomSens = baseScript.rightRearWheel.useCustomSensibility;
		wheelColliderListBase [2].customSens = baseScript.rightRearWheel.customSensibility;
		wheelColliderListBase[3]._wheelCollider = baseScript.leftRearWheel.wheelCollider;
		wheelColliderListBase [3].useCustomSens = baseScript.leftRearWheel.useCustomSensibility;
		wheelColliderListBase [3].customSens = baseScript.leftRearWheel.customSensibility;
		for (int x = 0; x < baseScript.otherWheels.Length; x++) {
			wheelColliderListBase [x + 4]._wheelCollider = baseScript.otherWheels[x].wheelCollider;
			wheelColliderListBase [x + 4].useCustomSens = baseScript.otherWheels[x].useCustomSensibility;
			wheelColliderListBase [x + 4].customSens = baseScript.otherWheels[x].customSensibility;
		}
		//
		wheelEmitterSoundX = new int[otherSounds.Length];
		wheelBlockSoundX = new int[otherSounds.Length];
		wheelEmitterSoundXSkid = new int[otherSounds.Length];
		wheelBlockSoundXSkid = new int[otherSounds.Length];
	}
	void Update () {
		KMhSounds = baseScript.KMh;
		wheelColliderListBase[0]._isSkidding = baseScript.rightFrontWheel.wheelSkidding;
		wheelColliderListBase[1]._isSkidding = baseScript.leftFrontWheel.wheelSkidding;
		wheelColliderListBase[2]._isSkidding = baseScript.rightRearWheel.wheelSkidding;
		wheelColliderListBase[3]._isSkidding = baseScript.leftRearWheel.wheelSkidding;
		for (int x = 0; x < baseScript.otherWheels.Length; x++) {
			wheelColliderListBase [x + 4]._isSkidding = baseScript.otherWheels[x].wheelSkidding;
		}
		//
		if (standardSound) {
			SkiddingSounds ();
		}
		if (otherSounds.Length > 0) {
			GroundSoundsEmitter (wheelColliderListBase);
		}
	}
	//

	void GenerateSKidSounds(){
		if (standardSound) {
			skiddingSoundAUD = GenerateAudioSourceSkid ("standardSkidSound", 10, 1, standardSound, false, false, false);
		}
		groundSoundsAUD = new AudioSource[otherSounds.Length];
		if (otherSounds.Length > 0) {
			for (int x = 0; x < otherSounds.Length; x++) {
				if (otherSounds [x].groundSound) {
					groundSoundsAUD [x] = GenerateAudioSourceSkid ("GroundSounds" + x, 10, otherSounds [x].volumeGroundSound, otherSounds [x].groundSound, true, false, false);
				}
			}
		}
		groundSoundsAUDSkid = new AudioSource[otherSounds.Length];
		if (otherSounds.Length > 0) {
			for (int x = 0; x < otherSounds.Length; x++) {
				if (otherSounds [x].skiddingSound && otherSounds [x].groundTag != null) {
					groundSoundsAUDSkid [x] = GenerateAudioSourceSkid ("GroundSoundsSkid" + x, 10, otherSounds [x].volume, otherSounds [x].skiddingSound, true, false, false);
				}
			}
		}
		skiddingSoundAUD.clip = standardSound;
	}

	public AudioSource GenerateAudioSourceSkid(string name, float minDistance, float volume, AudioClip audioClip, bool loop, bool playNow, bool playOnAwake){
		GameObject audioSource = new GameObject (name);
		audioSource.transform.position = transform.position;
		audioSource.transform.parent = transform;
		AudioSource temp = audioSource.AddComponent<AudioSource> () as AudioSource;
		temp.minDistance = minDistance;
		temp.volume = volume;
		temp.clip = audioClip;
		temp.loop = loop;
		temp.playOnAwake = playOnAwake;
		temp.spatialBlend = 1.0f;
		temp.dopplerLevel = 0.0f;
		if (playNow) {
			temp.Play ();
		}
		return temp;
	}

	void GroundSoundsEmitter(WheelListBase[] wheelCollidersList){
		for (int j = 0; j < otherSounds.Length; j++) {
			wheelEmitterSoundX [j] = 0;
			wheelBlockSoundX [j] = 0;
		}
		for (int i = 0; i < wheelCollidersList.Length; i++) {
			wheelCollidersList [i]._wheelCollider.GetGroundHit (out tempWheelHit);
			for (int x = 0; x < otherSounds.Length; x++) {
				if (otherSounds [x].groundSound && otherSounds [x].groundTag != null) {
					if (wheelCollidersList [i]._wheelCollider.isGrounded) {
						if (groundDetection == GroundSelect.useTag) {
							if (tempWheelHit.collider.gameObject.CompareTag(otherSounds [x].groundTag)) {
								wheelEmitterSoundX [x] ++;
								if (!groundSoundsAUD [x].isPlaying && groundSoundsAUD[x]) {
									groundSoundsAUD [x].PlayOneShot (groundSoundsAUD [x].clip);
								}
							} else {
								wheelBlockSoundX[x]++;
							}
						}
						else if (groundDetection == GroundSelect.usePhysicsMaterial) {
							if (tempWheelHit.collider.sharedMaterial == otherSounds [x].physicMaterial) {
								wheelEmitterSoundX [x] ++;
								if (!groundSoundsAUD [x].isPlaying && groundSoundsAUD[x]) {
									groundSoundsAUD [x].PlayOneShot (groundSoundsAUD [x].clip);
								}
							} else {
								wheelBlockSoundX[x]++;
							}
						}
					} else {
						wheelBlockSoundX[x]++;
					}
				}
			}
		}
		for (int x = 0; x < otherSounds.Length; x++) {
			if(wheelBlockSoundX[x] > 0 && wheelEmitterSoundX[x] == 0 && groundSoundsAUD[x]){
				groundSoundsAUD [x].Stop ();
			}
			if ((KMhSounds/3.6f) < 2.0f && groundSoundsAUD[x]) {
				groundSoundsAUD [x].Stop ();
			}
		}
	}

	void DiscoverMaxSkid(){
		sidewaysSlipMaxSkid = 0;
		forwardSlipMaxSkid = 0;
		if (baseScript.rightFrontWheel.wheelCollider.isGrounded) {
			baseScript.rightFrontWheel.wheelCollider.GetGroundHit (out tempWheelHit);
			compareSidewaysSkid = Mathf.Abs (tempWheelHit.sidewaysSlip);
			if (compareSidewaysSkid > sidewaysSlipMaxSkid) {
				sidewaysSlipMaxSkid = compareSidewaysSkid;
			}
			compareForwardSkid = Mathf.Abs (tempWheelHit.forwardSlip);
			if (compareForwardSkid > forwardSlipMaxSkid) {
				forwardSlipMaxSkid = compareForwardSkid;
			}
		}
		//
		if (baseScript.leftFrontWheel.wheelCollider.isGrounded) {
			baseScript.leftFrontWheel.wheelCollider.GetGroundHit (out tempWheelHit);
			compareSidewaysSkid = Mathf.Abs (tempWheelHit.sidewaysSlip);
			if (compareSidewaysSkid > sidewaysSlipMaxSkid) {
				sidewaysSlipMaxSkid = compareSidewaysSkid;
			}
			compareForwardSkid = Mathf.Abs (tempWheelHit.forwardSlip);
			if (compareForwardSkid > forwardSlipMaxSkid) {
				forwardSlipMaxSkid = compareForwardSkid;
			}
		}
		//
		if (baseScript.rightRearWheel.wheelCollider.isGrounded) {
			baseScript.rightRearWheel.wheelCollider.GetGroundHit (out tempWheelHit);
			compareSidewaysSkid = Mathf.Abs (tempWheelHit.sidewaysSlip);
			if (compareSidewaysSkid > sidewaysSlipMaxSkid) {
				sidewaysSlipMaxSkid = compareSidewaysSkid;
			}
			compareForwardSkid = Mathf.Abs (tempWheelHit.forwardSlip);
			if (compareForwardSkid > forwardSlipMaxSkid) {
				forwardSlipMaxSkid = compareForwardSkid;
			}
		}
		//
		if (baseScript.leftRearWheel.wheelCollider.isGrounded) {
			baseScript.leftRearWheel.wheelCollider.GetGroundHit (out tempWheelHit);
			compareSidewaysSkid = Mathf.Abs (tempWheelHit.sidewaysSlip);
			if (compareSidewaysSkid > sidewaysSlipMaxSkid) {
				sidewaysSlipMaxSkid = compareSidewaysSkid;
			}
			compareForwardSkid = Mathf.Abs (tempWheelHit.forwardSlip);
			if (compareForwardSkid > forwardSlipMaxSkid) {
				forwardSlipMaxSkid = compareForwardSkid;
			}
		}
		//
		for (int x = 0; x < baseScript.otherWheels.Length; x++) {
			if (baseScript.otherWheels [x].wheelCollider.isGrounded) {
				baseScript.otherWheels [x].wheelCollider.GetGroundHit (out tempWheelHit);
				compareSidewaysSkid = Mathf.Abs (tempWheelHit.sidewaysSlip);
				if (compareSidewaysSkid > sidewaysSlipMaxSkid) {
					sidewaysSlipMaxSkid = compareSidewaysSkid;
				}
				compareForwardSkid = Mathf.Abs (tempWheelHit.forwardSlip);
				if (compareForwardSkid > forwardSlipMaxSkid) {
					forwardSlipMaxSkid = compareForwardSkid;
				}
			}
		}
		//
		maxSlipTemp = 0;
		if (sidewaysSlipMaxSkid > forwardSlipMaxSkid) {
			maxSlipTemp = sidewaysSlipMaxSkid;
		} else {
			maxSlipTemp = forwardSlipMaxSkid;
		}
		//return 'maxSlipTemp' result
	}

	void SkiddingSounds(){
		DiscoverMaxSkid ();
		skiddingIsTrue = false;
		for (int x = 0; x < wheelColliderListBase.Length; x++) {
			if (wheelColliderListBase [x]._isSkidding) {
				skiddingIsTrue = true;
			}
		}
		//CHECK TAGS
		bool tempOtherGround = GroundSoundsEmitterSkid (wheelColliderListBase, maxSlipTemp);
		//standard sound play? \/ \/
		if (skiddingIsTrue) {
			if (!tempOtherGround) {//quer dizer que esta derrapando mas nao encontrou nenhuma tag
				if (skiddingSoundAUD.volume < (0.3f * standardVolume)) {
					skiddingSoundAUD.volume = Mathf.MoveTowards (skiddingSoundAUD.volume, (0.31f * standardVolume), Time.deltaTime * 4.0f);
				} else {
					skiddingSoundAUD.volume = Mathf.Lerp (skiddingSoundAUD.volume, (maxSlipTemp * standardVolume), Time.deltaTime * 7.0f);
				}
				if (!skiddingSoundAUD.isPlaying) {
					skiddingSoundAUD.Play ();
				}
			} else {
				skiddingSoundAUD.volume = Mathf.Lerp (skiddingSoundAUD.volume, 0, Time.deltaTime * 7.0f);
				if (skiddingSoundAUD.volume < 0.3f) {
					skiddingSoundAUD.Stop ();
				}
			}
		} else {
			skiddingSoundAUD.volume = Mathf.Lerp (skiddingSoundAUD.volume, 0, Time.deltaTime * 7.0f);
			if (skiddingSoundAUD.volume < 0.3f) {
				skiddingSoundAUD.Stop ();
			}
		}
	}

	bool GroundSoundsEmitterSkid(WheelListBase[] wheelCollidersList, float slipForVolume){
		for (int j = 0; j < otherSounds.Length; j++) {
			wheelEmitterSoundXSkid [j] = 0;
			wheelBlockSoundXSkid [j] = 0;
		}
		bool otherGround = false;
		wheelsInOtherGround = 0;
		maxWheels = 0;
		for (int i = 0; i < wheelCollidersList.Length; i++) {//WHEELS FOR
			if (wheelCollidersList [i]._wheelCollider.isGrounded) {
				maxWheels++;
				wheelCollidersList [i]._wheelCollider.GetGroundHit (out tempWheelHit);
				//
				for (int x = 0; x < otherSounds.Length; x++) {//SOUNDS FOR
					if (groundDetection == GroundSelect.useTag) {
						if (tempWheelHit.collider.gameObject.CompareTag (otherSounds [x].groundTag)) {
							wheelsInOtherGround++;
							if (wheelCollidersList[i]._isSkidding) {
								groundSoundsAUDSkid [x].volume = Mathf.Lerp (groundSoundsAUDSkid [x].volume, (slipForVolume * otherSounds [x].volume), Time.deltaTime * 5.0f);
								if (otherSounds [x].skiddingSound) {
									wheelEmitterSoundXSkid [x]++;
									if (!groundSoundsAUDSkid [x].isPlaying && groundSoundsAUDSkid [x]) {
										groundSoundsAUDSkid [x].PlayOneShot (groundSoundsAUDSkid [x].clip);
									}
								}
							} else {
								wheelBlockSoundXSkid [x]++;
							}
						} else {
							wheelBlockSoundXSkid [x]++;
						}
					} else if (groundDetection == GroundSelect.usePhysicsMaterial) {
						if (tempWheelHit.collider.sharedMaterial == otherSounds [x].physicMaterial) {
							wheelsInOtherGround++;
							if (wheelCollidersList[i]._isSkidding) {
								groundSoundsAUDSkid [x].volume = Mathf.Lerp (groundSoundsAUDSkid [x].volume, (slipForVolume * otherSounds [x].volume), Time.deltaTime * 5.0f);
								if (otherSounds [x].skiddingSound) {
									wheelEmitterSoundXSkid [x]++;
									if (!groundSoundsAUDSkid [x].isPlaying && groundSoundsAUDSkid [x]) {
										groundSoundsAUDSkid [x].PlayOneShot (groundSoundsAUDSkid [x].clip);
									}
								}
							} else {
								wheelBlockSoundXSkid [x]++;
							}
						} else {
							wheelBlockSoundXSkid [x]++;
						}
					}
				}
			}
		}
		for (int x = 0; x < otherSounds.Length; x++) {
			if(wheelBlockSoundXSkid[x] > 0 && wheelEmitterSoundXSkid[x] == 0 && groundSoundsAUDSkid[x]){
				groundSoundsAUDSkid [x].volume = Mathf.Lerp (groundSoundsAUDSkid [x].volume, 0, Time.deltaTime * 5.0f);
				if (groundSoundsAUDSkid [x].volume < 0.6f) {
					groundSoundsAUDSkid [x].Stop ();
				}
			}
		}
		if (wheelsInOtherGround == maxWheels) {
			otherGround = true;
		}
		return otherGround;
	}
}
