using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Base.Game
{
    public class InputControl : MonoBehaviour
    {
        public static event System.Action CallBegin;
        public static event System.Action CallEnd;

        [SerializeField] private float minLenght;

        [Range(90, 720)]
        [SerializeField] private float angleFactor = 200;

        private Vector2 oldPos;

        private bool isUpdate;

        public bool CheckBegin()
        {
            return Input.GetMouseButtonDown(0);
        }

        public void Begin()
        {
            isUpdate = true;
            oldPos = Input.mousePosition;
            CallBeginEv();
        }

        public bool CheckEnd()
        {
            return Input.GetMouseButtonUp(0);
        }

        public Vector2 GetTarget()
        {
            Vector2 dir = Input.mousePosition;
            if (oldPos == Vector2.zero && Vector2.Distance(dir, oldPos) >= minLenght)
            {
                return Vector2.zero;
            }
            else
            {
                return dir - oldPos;
            }
        }

        public void End()
        {
            isUpdate = false;
            oldPos = Vector2.zero;
            CallEndEv();
        }

        public void Clear()
        {
            oldPos = Vector2.zero;
            isUpdate = false;
        }

        private void CallBeginEv()
        {
            if (CallBegin != null)
            {
                CallBegin();
            }
        }
        private void CallEndEv()
        {
            if (CallEnd != null)
            {
                CallEnd();
            }
        }

        public Vector3 CalculateTargetCar(Vector3 targetStart, Vector2 mouseTarget)
        {
            float angle = ((-mouseTarget.x) / Screen.width * angleFactor) * Mathf.Deg2Rad;

            Vector2 matrixMul = new Vector2(targetStart.x * Mathf.Cos(angle) + (targetStart.y * -Mathf.Sin(angle)),
                targetStart.x * Mathf.Sin(angle) + targetStart.y * Mathf.Cos(angle));
            Vector3 target = new Vector3(matrixMul.x, 0, matrixMul.y);
            return target;
        }
    }
}