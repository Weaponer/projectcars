using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Base.Game
{
    public class Player : MonoBehaviour
    {
        public static Player Singleton;

        public event System.Action PlayerDead;

        [SerializeField] private InputControl input;

        [SerializeField] private float timeGodMode;

        private Car car;

        private bool isBlock;

        private bool isBlockInput;

        private bool isMoveCar;

        private Vector3 dirCarStart;

        private void Awake()
        {
            if (Singleton)
            {
                Destroy(gameObject);
                return;
            }
            else
            {
                Singleton = this;
            }
        }

        private void OnDestroy()
        {
            if (Singleton == this)
            {
                Singleton = null;
            }
        }

        private void Update()
        {
            if (!isBlock && !isBlockInput)
            {
                if (input.CheckBegin() && car.CheckIsCarRot())
                {
                    input.Begin();
                    isMoveCar = true;
                    car.FreezRot();
                    TimeControll.Singleton.ChannelTimeTo(TimeControll.SlowTime);
                    dirCarStart = new Vector2(car.transform.forward.x, car.transform.forward.z);
                }

                if (isMoveCar)
                {
                    Vector2 targetMon = input.GetTarget();
                    // Ray ray = CameraControl.CameraGame.ScreenPointToRay(targetMon);
                    if (targetMon != Vector2.zero)
                    {
                        //   Vector3 posMouse = InterSect(car.transform.position, car.transform.up, CameraControl.CameraGame.transform.position, ray.direction);
                        car.Rot(input.CalculateTargetCar(dirCarStart, targetMon));
                    }
                }

                if (input.CheckEnd())
                {
                    TimeControll.Singleton.ChannelTimeTo(TimeControll.TimeNormal);

                    isMoveCar = false;

                    if (car.CheckIsCarRot() && input.GetTarget() != Vector2.zero)
                    {
                        car.PushFromFreezRot();
                    }
                    input.Clear();
                    input.End();
                }
            }
            else if (isMoveCar)
            {
                input.End();
                isMoveCar = false;
                input.Clear();
                car.EndFreezRot();
                TimeControll.Singleton.ChannelTimeTo(TimeControll.TimeNormal);
            }
        }

        /*        private Vector3 InterSect(Vector3 a, Vector3 n, Vector3 posRay, Vector3 directRay)
                {
                    directRay = -directRay;
                    // Vector3 n = Vector3.Normalize(Vector3.Cross(e1, e2));

                    float dot = Vector3.Dot(n, directRay);


                    float d = Vector3.Dot(n, a);

                    float t = d - Vector3.Dot(n, posRay);


                    if (t >= 0f || t >= dot)
                    {
                        return Vector3.zero;
                    }
                    t /= dot;

                    Vector3 p = posRay + directRay * t;
                    return p;
                }*/

        public void SetCar(Car prefabCar)
        {
            if (car)
                Destroy(car.gameObject);
            car = Instantiate(prefabCar, Vector3.zero, Quaternion.identity);
            car.transform.parent = transform;
            car.DamagCar += CallDead;
            PlayerSetPosition(transform.position, transform.rotation);
        }

        public void ActiveGodMove()
        {
            car.GodMode(timeGodMode);
        }

        public void PlayerSetPosition(Vector3 vec, Quaternion rot)
        {
            transform.position = vec;
            transform.rotation = rot;
            car.transform.localPosition = Vector3.zero;
            car.transform.localRotation = Quaternion.identity;
            CameraControl.Singleton.SetCameraToCar(car);
            car.ResetCar();
        }

        public Vector3 GetPosition()
        {
            return car.transform.position;
        }

        public void SetLockPlayer()
        {
            car.FreezPhys();
            isBlock = true;
            input.Clear();
        }

        public void UnLockPlayer()
        {
            car.UnFreezPhys();
            isBlock = false;
        }

        private void CallDead()
        {
            car.DestroyCar();
            if (PlayerDead != null)
            {
                PlayerDead();
            }
        }

        public void LockInput()
        {
            isBlockInput = true;
        }

        public void UnLockInput()
        {
            isBlockInput = false;
        }
    }


}