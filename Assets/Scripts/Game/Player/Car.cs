using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace Base.Game
{
    public class Car : MonoBehaviour
    {
        public event System.Action DamagCar;
     

        [SerializeField] private float pushMotorSize;
        [SerializeField] private float pushForceSize;

        [SerializeField] private float frictionWheel;

        [Range(0, 40)]
        [SerializeField] private float speedRot;

        [Space(15)]

        [SerializeField] private GameObject[] models;

        [SerializeField] private Rigidbody rigidbody;

        [SerializeField] private BoxCollider trigerColliderGround;

        [SerializeField] private GameObject lightObject;

        [SerializeField] private WheelCollider[] forwardWheelCol;
        [SerializeField] private WheelCollider[] rearWheelCol;

        [SerializeField] private Transform[] forwardWheelModel;
        [SerializeField] private Transform[] rearWheelModel;

        [Space(15)]
        [SerializeField] private UnityEvent carDestoy;

        [SerializeField] private UnityEvent carReset;

        [SerializeField] private UnityEvent moveCar;


        private bool isClearMotor;
        private bool isClearMotorLateUpdate;

        private Quaternion moveRot;

        private bool isRot;

        private void Awake()
        {
            rigidbody.centerOfMass = Vector3.zero;
            return;
            Map.SunControl.StartDay += OffLight;
            Map.SunControl.StartNight += OnLight;
        }

        private void Update()
        {
            if (!rigidbody.isKinematic)
            {
                if (isClearMotorLateUpdate)
                {
                    for (int i = 0; i < rearWheelCol.Length; i++)
                    {
                        rearWheelCol[i].motorTorque = 0;
                    }
                    isClearMotorLateUpdate = false;
                }
                Vector3 pos;
                Quaternion rot;
                for (int i = 0; i < rearWheelCol.Length; i++)
                {
                    rearWheelCol[i].GetWorldPose(out pos, out rot);
                    rearWheelModel[i].position = pos;
                    rearWheelModel[i].rotation = rot;
                }
                for (int i = 0; i < forwardWheelCol.Length; i++)
                {
                    forwardWheelCol[i].GetWorldPose(out pos, out rot);
                    forwardWheelModel[i].position = pos;
                    forwardWheelModel[i].rotation = rot;
                }

                if (isRot)
                {
                    transform.rotation = Quaternion.Slerp(transform.rotation, moveRot, Mathf.Min(1, (speedRot / (Quaternion.Angle(transform.rotation, moveRot) / 180f)) * Time.deltaTime));
                }
            }
        }

        private void LateUpdate()
        {
            if (isClearMotor)
            {
                isClearMotor = false;
                isClearMotorLateUpdate = true;
            }
        }

        private void OffLight()
        {
            lightObject.SetActive(false);
        }

        private void OnLight()
        {
            lightObject.SetActive(true);
        }

        public bool CheckIsCarRot()
        {
            LayerMask mask = LayerMask.GetMask("Ground");
            return Physics.CheckBox(transform.TransformVector(trigerColliderGround.center) + transform.position, trigerColliderGround.size, transform.rotation, mask, QueryTriggerInteraction.Collide);
        }

        public void ResetCar()
        {
            for (int i = 0; i < models.Length; i++)
            {
                models[i].SetActive(true);
            }

            carReset.Invoke();
        }

        public void DestroyCar()
        {
            for (int i2 = 0; i2 < models.Length; i2++)
            {
                models[i2].SetActive(false);
            }
            carDestoy.Invoke();
        }

        public void GodMode(float time)
        {
            StartCoroutine(GodModeWork(time, 0.3f));
        }

        IEnumerator GodModeWork(float time, float repetTime)
        {
            float countTime = 0;
            gameObject.layer = LayerMask.NameToLayer("CarPlayerGod");
            while (countTime < time)
            {
                countTime += repetTime;
                DestroyCar();
                yield return new WaitForSecondsRealtime(repetTime);
                ResetCar();
                countTime += repetTime;
                yield return new WaitForSecondsRealtime(repetTime);
            }
            gameObject.layer = LayerMask.NameToLayer("CarPlayer");
        }

        public void FreezPhys()
        {
            rigidbody.isKinematic = true;
        }

        public void FreezRot()
        {
            for (int i = 0; i < rearWheelCol.Length; i++)
            {
                rearWheelCol[i].motorTorque = 0;
            }
            SetFriction(0, 0);
            isRot = true;
            moveRot = transform.rotation;
        }

        public void EndFreezRot()
        {
            SetFriction(frictionWheel, frictionWheel);
            isRot = false;
        }


        public void Rot(Vector3 direct)
        {
            Quaternion rot = Quaternion.LookRotation(direct.normalized, transform.up);

            moveRot = rot;
        }

        public void PushFromFreezRot()
        {
            EndFreezRot();
            for (int i = 0; i < rearWheelCol.Length; i++)
            {
                rearWheelCol[i].motorTorque = pushMotorSize;
            }
            rigidbody.velocity = Vector3.zero;
            rigidbody.angularVelocity = Vector3.zero;
            rigidbody.AddForce(transform.forward * pushForceSize, ForceMode.VelocityChange);
            isClearMotor = true;
            isRot = false;

            moveCar.Invoke();
        }

        public void UnFreezPhys()
        {
            rigidbody.isKinematic = false;
        }

        private void SetFriction(float frictionForward, float frictionSide)
        {
            WheelFrictionCurve frictionCurve = new WheelFrictionCurve();
            frictionCurve.asymptoteSlip = 0.8f;
            frictionCurve.asymptoteValue = 0.5f;
            frictionCurve.extremumValue = 1f;
            frictionCurve.extremumSlip = 0.4f;
            for (int i = 0; i < forwardWheelCol.Length; i++)
            {
                frictionCurve.stiffness = frictionForward;
                forwardWheelCol[i].forwardFriction = frictionCurve;
                frictionCurve.stiffness = frictionSide;
                forwardWheelCol[i].sidewaysFriction = frictionCurve;
            }
            for (int i = 0; i < rearWheelCol.Length; i++)
            {
                frictionCurve.stiffness = frictionForward;
                rearWheelCol[i].forwardFriction = frictionCurve;
                frictionCurve.stiffness = frictionSide;
                rearWheelCol[i].sidewaysFriction = frictionCurve;
            }
        }

        private void OnCollisionEnter(Collision collision)
        {
            for (int i = 0; i < collision.contactCount; i++)
            {
                int layer = collision.GetContact(i).otherCollider.gameObject.layer;
                if (layer == LayerMask.NameToLayer("EnemyObj") || layer == LayerMask.NameToLayer("BorderEnemy"))
                {

                    if (DamagCar != null)
                    {
                        DamagCar();
                    }
                    return;
                }
            }
        }

        private void OnDestroy()
        {
            Map.SunControl.StartDay -= OffLight;
            Map.SunControl.StartNight -= OnLight;
        }
    }
}