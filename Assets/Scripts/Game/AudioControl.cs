using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Base.Process
{
    public class AudioControl
    {
        public static event System.Action StopMusic;
        public static event System.Action StartMusic;

        public static event System.Action StartSound;
        public static event System.Action StopSound;

        public static void SetActiveMusic(bool isActive)
        {
            LoadStatusPlayer.Status.Music = isActive;
            if (isActive)
            {
                if (StartMusic != null)
                {
                    StartMusic();
                }
            }
            else
            {
                if (StopMusic != null)
                {
                    StopMusic();
                }
            }
        }

        public static bool IsActiveMusic()
        {
            return LoadStatusPlayer.Status.Music;
        }
        public static bool IsActiveSound()
        {
            return LoadStatusPlayer.Status.Sound;
        }


        public static void SetActiveSound(bool isActive)
        {
            LoadStatusPlayer.Status.Sound = isActive;
            if (isActive)
            {
                if (StartSound != null)
                {
                    StartSound();
                }
            }
            else
            {
                if (StopSound != null)
                {
                    StopSound();
                }
            }
        }
    }
}