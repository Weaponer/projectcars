using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Base.Process.Task
{
    [System.Serializable]
    public class GetMoneyData : TaskData
    {
        public int CountMoneyMax;
    }
}