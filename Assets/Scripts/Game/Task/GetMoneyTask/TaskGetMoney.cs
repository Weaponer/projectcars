using Base.Game.UI;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Base.Process;
using Base.Game;

namespace Base.Process.Task
{
    public class TaskGetMoney : TaskObject
    {
        [SerializeField] private int countMoney;

        public override TaskData GenerateData()
        {
            return new GetMoneyData();
        }

        public override void StartTask(TaskData data)
        {
            base.StartTask(data);
            Debug.Log("Start get money");
            if (!Data.IsEnd)
            {
                GameProcess.CallPlayerDead += ShowEndGame;
                GameProcess.CallEndGame += EndGame;
            }
        }

        public override bool IsCurrentTypeDate(TaskData data)
        {
            return data.GetType() == typeof(GetMoneyData);
        }

        protected override void CallShowTask(TaskUI taskUI)
        {
            ((TaskUICount)taskUI).SetParams("�������� " + countMoney + " ����� �� �����", countMoney, ((GetMoneyData)Data).CountMoneyMax, addMoney);
        }

        private void ShowEndGame()
        {
            if (GameProcess.ParamsGame.MoneyCount > ((GetMoneyData)Data).CountMoneyMax)
            {
                ((GetMoneyData)Data).CountMoneyMax = Mathf.Min(countMoney, GameProcess.ParamsGame.MoneyCount);
            }
        }

        private void EndGame()
        {
            if (((GetMoneyData)Data).CountMoneyMax == countMoney)
            {
                Data.IsEnd = true;
                GameProcess.ParamsGame.MoneyCount += addMoney;

                GameProcess.CallPlayerDead -= ShowEndGame;
                GameProcess.CallEndGame -= EndGame;
            }
        }
    }
}