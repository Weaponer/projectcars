using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Base.Process
{
    public abstract class TaskObject : MonoBehaviour
    {
        public TaskData Data { get; private set; }

        [SerializeField] protected Task.TaskUI taskUI;

        [SerializeField] protected int addMoney;

        public virtual TaskData GenerateData()
        {
            return null;
        }

        public virtual void StartTask(TaskData data)
        {
            Data = data;
        }

        public virtual bool IsCurrentTypeDate(TaskData data)
        {
            return false;
        }

        public virtual bool IsShowTask()
        {
            if (Data.IsEnd)
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        public Task.TaskUI ShowTask(Transform point)
        {
            Task.TaskUI task = Instantiate(taskUI);
            task.ShowTask(Data.IsEnd, point);

            CallShowTask(task);
            return task;
        }

        protected virtual void CallShowTask(Task.TaskUI taskUI)
        {

        }
    }
}