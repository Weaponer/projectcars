using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Base.Process.Task;

namespace Base.Game.UI
{
    public class TaskUICount : TaskUI
    {

        [SerializeField] private GameObject checkObj;

        [SerializeField] private Image slider;
        [SerializeField] private Text content;

        [SerializeField] private Text addMoneyCount;

        [SerializeField] private Text setCount;

        public void SetParams(string contentName, int max, int count, int addMoney)
        {
            setCount.text = count + "/" + max;
            addMoneyCount.text = "+" + addMoney;
            content.text = contentName;
            slider.fillAmount = (float)count / max;
        }

        protected override void ShowEnd(Transform point)
        {
            checkObj.SetActive(true);
        }

        protected override void ShowNoEnd(Transform point)
        {
            checkObj.SetActive(false);
        }
    }
}