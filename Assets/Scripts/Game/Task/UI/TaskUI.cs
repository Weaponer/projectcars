using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Base.Process.Task
{
    public class TaskUI : MonoBehaviour
    {

        [SerializeField] protected GameObject panel;

        public void ShowTask(bool isEnd, Transform point)
        {
            panel.transform.parent = point;
            panel.transform.localPosition = Vector3.zero;
            panel.SetActive(true);
            if (isEnd)
            {
                ShowEnd(point);
            }
            else
            {
                ShowNoEnd(point);
            }
        }

        protected virtual void ShowEnd(Transform point)
        {
        }

        protected virtual void ShowNoEnd(Transform point)
        {
        }
    }
}