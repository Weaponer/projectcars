using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Base.Process.Task
{
    [System.Serializable]
    public class ConnectRailData : TaskData
    {
        public int CountMaxGet;
        public int CountGet;
    }
}