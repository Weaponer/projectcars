using Base.Game.UI;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Base.Process;
using Base.Game;
using Base.Game.Map;

namespace Base.Process.Task
{
    public class TaskConnectRail : TaskObject
    {
        [SerializeField] private int countRail;

        private int count;

        private ConnectRailData getRailData
        {
            get
            {
                return (ConnectRailData)Data;
            }
        }

        public override TaskData GenerateData()
        {
            ConnectRailData data = new ConnectRailData();
            data.CountGet = countRail;
            return data;
        }

        public override void StartTask(TaskData data)
        {
            base.StartTask(data);
            if (!Data.IsEnd)
            {
                GameProcess.CallPlayerDead += ShowEndGame;
                GameProcess.CallEndGame += EndGame;
                StageTrain.CallPlayerMoveTo += AddRail;
                count = 0;
            }
        }


        public override bool IsCurrentTypeDate(TaskData data)
        {
            return data.GetType() == typeof(ConnectRailData) && ((ConnectRailData)data).CountGet == countRail;
        }

        protected override void CallShowTask(TaskUI taskUI)
        {
            ((TaskUICount)taskUI).SetParams("�������� �� " + countRail + " ������� �� ����.", countRail, getRailData.CountMaxGet, addMoney);
        }

        private void ShowEndGame()
        {
            if (count > getRailData.CountMaxGet)
            {
                getRailData.CountMaxGet = Mathf.Min(countRail, count);
            }
        }

        private void EndGame()
        {
            if (getRailData.CountMaxGet == countRail)
            {
                Data.IsEnd = true;
                GameProcess.ParamsGame.MoneyCount += addMoney;

                GameProcess.CallPlayerDead -= ShowEndGame;
                GameProcess.CallEndGame -= EndGame;
                StageTrain.CallPlayerMoveTo -= AddRail;
            }
        }

        private void AddRail() {
            count++;
        }
    }
}