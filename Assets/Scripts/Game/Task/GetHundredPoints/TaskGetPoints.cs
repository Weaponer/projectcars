using Base.Game.UI;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Base.Process;
using Base.Game;

namespace Base.Process.Task
{
    public class TaskGetPoints : TaskObject
    {
        [SerializeField] private int countPoints;

        private GetPointsData getPointsData
        {
            get
            {
                return (GetPointsData)Data;
            }
        }

        public override TaskData GenerateData()
        {
            GetPointsData data = new GetPointsData();
            data.CountGet = countPoints;
            return data;
        }

        public override void StartTask(TaskData data)
        {
            base.StartTask(data);
            Debug.Log("Start get points " + data.IsEnd);
            if (!Data.IsEnd)
            {
                GameProcess.CallPlayerDead += ShowEndGame;
                GameProcess.CallEndGame += EndGame;
            }
        }


        public override bool IsCurrentTypeDate(TaskData data)
        {
            return data.GetType() == typeof(GetPointsData) && ((GetPointsData)data).CountGet == countPoints;
        }

        protected override void CallShowTask(TaskUI taskUI)
        {
            ((TaskUICount)taskUI).SetParams("�������� " + countPoints + " ����� �� �����", countPoints, getPointsData.CountMaxGet, addMoney);
        }

        private void ShowEndGame()
        {
            if (GameProcess.ParamsGame.ScoreCount > getPointsData.CountMaxGet)
            {
                getPointsData.CountMaxGet = Mathf.Min(countPoints, GameProcess.ParamsGame.ScoreCount);
            }
        }

        private void EndGame()
        {
            if (getPointsData.CountMaxGet == countPoints)
            {
                Data.IsEnd = true;
                GameProcess.ParamsGame.MoneyCount += addMoney;

                GameProcess.CallPlayerDead -= ShowEndGame;
                GameProcess.CallEndGame -= EndGame;
            }
        }
    }
}