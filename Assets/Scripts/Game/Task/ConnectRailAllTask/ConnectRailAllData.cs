using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Base.Process.Task
{
    [System.Serializable]
    public class ConnectRailAllData : TaskData
    {
        public int CountMaxGetAll;
        public int CountGetAll;
    }
}