using Base.Game.UI;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Base.Process;
using Base.Game;
using Base.Game.Map;

namespace Base.Process.Task
{
    public class TaskConnectRailAll : TaskObject
    {
        [SerializeField] private int countTrailAll;

        private ConnectRailAllData getTrailAllData
        {
            get
            {
                return (ConnectRailAllData)Data;
            }
        }

        public override TaskData GenerateData()
        {
            ConnectRailAllData data = new ConnectRailAllData();
            data.CountGetAll = countTrailAll;
            return data;
        }

        public override void StartTask(TaskData data)
        {
            base.StartTask(data);
            if (!Data.IsEnd)
            {
                GameProcess.CallEndGame += EndGame;
                StageTrain.CallPlayerMoveTo += AddRail;
            }
        }


        public override bool IsCurrentTypeDate(TaskData data)
        {
            return data.GetType() == typeof(ConnectRailAllData) && ((ConnectRailAllData)data).CountGetAll == countTrailAll;
        }

        protected override void CallShowTask(TaskUI taskUI)
        {
            getTrailAllData.CountMaxGetAll = Mathf.Min(countTrailAll, getTrailAllData.CountMaxGetAll);
            ((TaskUICount)taskUI).SetParams("�������� �� " + countTrailAll + " ������� �� ����.", countTrailAll, getTrailAllData.CountMaxGetAll, addMoney);
        }


        private void EndGame()
        {
            getTrailAllData.CountMaxGetAll = Mathf.Min(countTrailAll, getTrailAllData.CountMaxGetAll);
            if (getTrailAllData.CountMaxGetAll == countTrailAll)
            {
                Data.IsEnd = true;
                GameProcess.ParamsGame.MoneyCount += addMoney;
                GameProcess.CallEndGame -= EndGame;
            }
        }

        private void AddRail()
        {
            getTrailAllData.CountMaxGetAll++;
        }
    }
}