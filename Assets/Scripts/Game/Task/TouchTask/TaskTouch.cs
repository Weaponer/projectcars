using Base.Game.UI;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Base.Process;
using Base.Game;
using Base.Game.Map;
using System;

namespace Base.Process.Task
{
    public class TaskTouch : TaskObject
    {
        [SerializeField] private int countSecond;

        private DateTime timer;

        private bool isStop;

        private double countSec;

        private TouchData getNoTouchData
        {
            get
            {
                return (TouchData)Data;
            }
        }

        public override TaskData GenerateData()
        {
            TouchData data = new TouchData();
            data.CountSecond = countSecond;
            return data;
        }

        public override void StartTask(TaskData data)
        {
            base.StartTask(data);
            if (!Data.IsEnd)
            {
                GameProcess.CallEndGame += EndGame;
                GameProcess.CallStartGame += StartGame;
            }
        }


        public override bool IsCurrentTypeDate(TaskData data)
        {
            return data.GetType() == typeof(TouchData) && ((TouchData)data).CountSecond == countSecond;
        }

        protected override void CallShowTask(TaskUI taskUI)
        {

            ((TaskUICount)taskUI).SetParams("������� ����� �� ������ �� ����� " + countSecond + " ������.", countSecond, getNoTouchData.CountMaxSecond, addMoney);
        }


        private void StartGame()
        {
            InputControl.CallBegin += Clk;
            InputControl.CallEnd += ClkEnd;
            GameProcess.CallContinueGame += Next;
            GameProcess.CallPlayerDead += Stop;
            Next();
            countSec = 0;
        }

        private void EndGame()
        {
            if (getNoTouchData.CountMaxSecond == countSecond)
            {
                Data.IsEnd = true;
                GameProcess.ParamsGame.MoneyCount += addMoney;
                GameProcess.CallEndGame -= EndGame;
                GameProcess.CallStartGame -= StartGame;
            }
            InputControl.CallBegin -= Clk;
            InputControl.CallEnd -= ClkEnd;
            GameProcess.CallContinueGame -= Next;
            GameProcess.CallPlayerDead -= Stop;
        }

        private void Stop()
        {
            isStop = true;
        }

        private void Next()
        {
            isStop = false;
        }

        private void Clk()
        {
            timer = DateTime.Now;
        }

        private void ClkEnd()
        {
            if (isStop)
            {
                timer = DateTime.Now;
                return;
            }

            DateTime now = DateTime.Now;
            double sec = (now - timer).TotalSeconds;
            countSec += sec;
            if (countSec >= countSecond)
            {
                getNoTouchData.CountMaxSecond = countSecond;
            }
            else if (countSec > getNoTouchData.CountMaxSecond)
            {
                getNoTouchData.CountMaxSecond = (int)countSec;
            }
        }
    }
}