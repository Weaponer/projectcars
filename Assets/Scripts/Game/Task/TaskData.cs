using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Base.Process
{
    [System.Serializable]
    public class TaskData
    {
        public bool IsEnd = false;
    }
}