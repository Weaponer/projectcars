using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Base.Process;
using Base.Game;
using Base.Game.Map;
using Base.Game.UI;


namespace Base.Process.Task
{
    public class TaskBumpPillar : TaskObject
    {
        [SerializeField] private int countPillar;

        public override TaskData GenerateData()
        {
            return new BumpPillarData();
        }

        public override bool IsCurrentTypeDate(TaskData data)
        {
            return data.GetType() == typeof(BumpPillarData);
        }

        public override void StartTask(TaskData data)
        {
            base.StartTask(data);
            if (!Data.IsEnd)
            {
                Pillar.PlayerBumpPillar += BumpPillar;
            }
        }

        protected override void CallShowTask(TaskUI taskUI)
        {
            ((TaskUICount)taskUI).SetParams("������ " + countPillar + " �������", countPillar, ((BumpPillarData)Data).CountBumpPillar, addMoney);
        }

        private void BumpPillar()
        {
            BumpPillarData d = ((BumpPillarData)Data);
            d.CountBumpPillar++;
            if (d.CountBumpPillar == countPillar)
            {
                d.IsEnd = true;
                Pillar.PlayerBumpPillar -= BumpPillar;
            }
        }
    }
}