using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace Base.Process.Task
{
    [System.Serializable]
    public class BumpPillarData : TaskData
    {
        public int CountBumpPillar;
    }
}