using Base.Game.UI;
using Base.UI;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Base.Process;
using Base.Game;

namespace Base.Process.Task
{
    public class TaskPlayOnRoulette : TaskObject
    {
        public override bool IsCurrentTypeDate(TaskData data)
        {
            return data.GetType() == typeof(PlayOnRouletteData);
        }

        public override bool IsShowTask()
        {
            return false;
        }

        public override TaskData GenerateData()
        {
            return new PlayOnRouletteData();
        }

        protected override void CallShowTask(TaskUI taskUI)
        {
            if (Data.IsEnd)
            {
                ((TaskUICount)taskUI).SetParams("�������� � �������.", 1, 1, addMoney);
            }
            else
            {
                ((TaskUICount)taskUI).SetParams("�������� � �������.", 1, 0, addMoney);
            }
        }

        public override void StartTask(TaskData data)
        {
            base.StartTask(data);
            if (!Data.IsEnd)
            {
                RoulettePanelUI.CallPlay += CallPlay;
            }
        }

        private void CallPlay()
        {
            RoulettePanelUI.CallPlay -= CallPlay;
            Data.IsEnd = true;
            LoadStatusPlayer.Status.CountMoney += addMoney;
        }
    }
}