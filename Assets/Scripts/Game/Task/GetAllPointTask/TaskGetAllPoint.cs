using Base.Game.UI;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Base.Process;
using Base.Game;

namespace Base.Process.Task
{
    public class TaskGetAllPoint : TaskObject
    {
        [SerializeField] private int countPointsAll;

        private int countScoreGame = 0;

        private GetAllPointData getPointsAllData
        {
            get
            {
                return (GetAllPointData)Data;
            }
        }

        public override TaskData GenerateData()
        {
            GetAllPointData data = new GetAllPointData();
            data.CountGetAll = countPointsAll;
            return data;
        }

        public override void StartTask(TaskData data)
        {
            base.StartTask(data);
            if (!Data.IsEnd)
            {
                GameProcess.CallPlayerDead += ShowEndGame;
                GameProcess.CallEndGame += EndGame;
            }
        }


        public override bool IsCurrentTypeDate(TaskData data)
        {
            return data.GetType() == typeof(GetAllPointData) && ((GetAllPointData)data).CountGetAll == countPointsAll;
        }

        protected override void CallShowTask(TaskUI taskUI)
        {
            countScoreGame = Mathf.Min(countPointsAll, GameProcess.ParamsGame.ScoreCount + getPointsAllData.CountMaxGetAll);
            ((TaskUICount)taskUI).SetParams("�������� " + countPointsAll + " �����.", countPointsAll, countScoreGame, addMoney);
        }

        private void ShowEndGame()
        {

        }

        private void EndGame()
        {
            getPointsAllData.CountMaxGetAll = Mathf.Min(countPointsAll, GameProcess.ParamsGame.ScoreCount + getPointsAllData.CountMaxGetAll);
            if (getPointsAllData.CountMaxGetAll == countPointsAll)
            {
                Data.IsEnd = true;
                GameProcess.ParamsGame.MoneyCount += addMoney;

                GameProcess.CallPlayerDead -= ShowEndGame;
                GameProcess.CallEndGame -= EndGame;
            }
        }
    }
}