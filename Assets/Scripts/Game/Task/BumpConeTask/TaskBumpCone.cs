using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Base.Process;
using Base.Game;
using Base.Game.Map;
using Base.Game.UI;

namespace Base.Process.Task
{
    public class TaskBumpCone : TaskObject
    {
        [SerializeField] private int countCone;


        public override TaskData GenerateData()
        {
            return new BumpConeData();
        }

        public override bool IsCurrentTypeDate(TaskData data)
        {
            return data.GetType() == typeof(BumpConeData);
        }

        public override void StartTask(TaskData data)
        {
            base.StartTask(data);
            if (!Data.IsEnd)
            {
                Cone.PlayerBumpCone += BumpCone;
            }
        }

        protected override void CallShowTask(TaskUI taskUI)
        {
            ((TaskUICount)taskUI).SetParams("������ " + countCone + " �������", countCone, ((BumpConeData)Data).CountBumpCone, addMoney);
        }

        private void BumpCone() {
            BumpConeData d = ((BumpConeData)Data);
            d.CountBumpCone++;
            if (d.CountBumpCone == countCone) {
                d.IsEnd = true;
                Cone.PlayerBumpCone -= BumpCone;
            }
        }
    }
}