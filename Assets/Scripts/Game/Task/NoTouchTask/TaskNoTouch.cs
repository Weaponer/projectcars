using Base.Game.UI;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Base.Process;
using Base.Game;
using Base.Game.Map;
using System;

namespace Base.Process.Task
{
    public class TaskNoTouch : TaskObject
    {
        [SerializeField] private int countSecond;

        private int count;

        private DateTime timer;

        private bool isStop;

        private NoTouchData getNoTouchData
        {
            get
            {
                return (NoTouchData)Data;
            }
        }

        public override TaskData GenerateData()
        {
            NoTouchData data = new NoTouchData();
            data.CountSecond = countSecond;
            return data;
        }

        public override void StartTask(TaskData data)
        {
            base.StartTask(data);
            if (!Data.IsEnd)
            {
                GameProcess.CallEndGame += EndGame;
                GameProcess.CallStartGame += StartGame;
            }
        }


        public override bool IsCurrentTypeDate(TaskData data)
        {
            return data.GetType() == typeof(NoTouchData) && ((NoTouchData)data).CountSecond == countSecond;
        }

        protected override void CallShowTask(TaskUI taskUI)
        {

            ((TaskUICount)taskUI).SetParams("�� ��������� " + countSecond + " ������.", countSecond, getNoTouchData.CountMaxSecond, addMoney);
        }


        private void StartGame()
        {
            InputControl.CallBegin += Clk;
            InputControl.CallEnd += ClkEnd;
            GameProcess.CallContinueGame += Next;
            GameProcess.CallPlayerDead += Stop;
            count = 0;
            Next();
        }

        private void EndGame()
        {
            if (getNoTouchData.CountMaxSecond == countSecond)
            {
                Data.IsEnd = true;
                GameProcess.ParamsGame.MoneyCount += addMoney;
                GameProcess.CallEndGame -= EndGame;
                GameProcess.CallStartGame -= StartGame;
            }
            InputControl.CallBegin -= Clk;
            InputControl.CallEnd -= ClkEnd;
            GameProcess.CallContinueGame -= Next;
            GameProcess.CallPlayerDead -= Stop;
        }

        private void Stop()
        {
            isStop = true;
        }

        private void Next()
        {
            isStop = false;
        }

        private void Clk()
        {
            if (isStop) {
                timer = DateTime.Now;
                return;
            }
            if (count > 1)
            {
                DateTime now = DateTime.Now;
                double sec = (now - timer).TotalSeconds;
                if (sec >= countSecond)
                {
                    getNoTouchData.CountMaxSecond = countSecond;
                }
                else if (sec > getNoTouchData.CountMaxSecond)
                {
                    getNoTouchData.CountMaxSecond = (int)sec;
                }
            }
        }

        private void ClkEnd()
        {
            count++;
            timer = DateTime.Now;
        }
    }
}