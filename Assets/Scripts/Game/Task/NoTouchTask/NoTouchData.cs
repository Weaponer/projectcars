using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Base.Process.Task
{
    [System.Serializable]
    public class NoTouchData : TaskData
    {
        public int CountMaxSecond;
        public int CountSecond;
    }
}