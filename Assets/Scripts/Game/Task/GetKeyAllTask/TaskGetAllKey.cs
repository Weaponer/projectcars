using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Base.Process;
using Base.Game;
using Base.Game.UI;

namespace Base.Process.Task
{
    public class TaskGetAllKey : TaskObject
    {
        [SerializeField] private int countKeyAll;

        private int countStartGame = 0;

        private GetAllKeyData getKeysAllData
        {
            get
            {
                return (GetAllKeyData)Data;
            }
        }

        public override TaskData GenerateData()
        {
            GetAllKeyData data = new GetAllKeyData();
            data.CountGetAll = countKeyAll;
            return data;
        }

        public override bool IsCurrentTypeDate(TaskData data)
        {
            return data.GetType() == typeof(GetAllKeyData) && ((GetAllKeyData)data).CountGetAll == countKeyAll;
        }

        public override void StartTask(TaskData data)
        {
            base.StartTask(data);
            if (!Data.IsEnd)
            {
                GameProcess.CallPlayerDead += ShowEndGame;
                GameProcess.CallEndGame += EndGame;
                GameProcess.CallStartGame += StartGame;
            }
        }

        protected override void CallShowTask(TaskUI taskUI)
        {
            ((TaskUICount)taskUI).SetParams("�������� " + countKeyAll + " ������.", countKeyAll,
               Mathf.Min(countKeyAll, getKeysAllData.CountMaxGetAll + (LoadStatusPlayer.Status.CountKey - countStartGame)), addMoney);
        }

        private void StartGame()
        {
            countStartGame = LoadStatusPlayer.Status.CountKey;
        }

        private void ShowEndGame()
        {

        }

        private void EndGame()
        {
            getKeysAllData.CountMaxGetAll = Mathf.Min(countKeyAll, getKeysAllData.CountMaxGetAll + (LoadStatusPlayer.Status.CountKey - countStartGame));
            if (getKeysAllData.CountMaxGetAll == countKeyAll)
            {
                Data.IsEnd = true;
                GameProcess.ParamsGame.MoneyCount += addMoney;

                GameProcess.CallPlayerDead -= ShowEndGame;
                GameProcess.CallEndGame -= EndGame;
                GameProcess.CallStartGame -= StartGame;
            }
        }
    }
}