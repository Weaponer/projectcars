using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Base.UI;
using Base.Process;

namespace Base.Game
{
    public class GameProcess : MonoBehaviour
    {
        public static AcamulatorGameParams ParamsGame { get; private set; }

        public static GameProcess Singleton { get; private set; }

        public static event System.Action CallStartGame;

        public static event System.Action CallEndGame;

        public static event System.Action CallContinueGame;

        public static event System.Action CallPlayerDead;

        [SerializeField] private MainMenu mainMenu;
        [SerializeField] private GameUI gameUI;
        [SerializeField] private Map.MapGenerator mapGenerator;

        [SerializeField] private Player player;
        [SerializeField] private CarsDataBase carsData;

        [Space(15)]
        [SerializeField] private int addScoreStage = 2;

        public void InitGame()
        {
            Singleton = this;

            mainMenu.StartGameClk += StartGame;
            ShopPanelUI.CarTake += CallTakeCar;

            SetCarPlayer(carsData.GetCars()[LoadStatusPlayer.Status.TakeCar]);

            ParamsGame = new AcamulatorGameParams();
            ParamsGame.MaxScoreCount = LoadStatusPlayer.Status.MaxScore;


            if (!LoadStatusPlayer.Status.TutorialEnd)
            {
                StartGameTutorial();
            }
            else
            {
                mainMenu.OpenMenu();
                GenerateStart();
            }
        }

        public static void AddScore()
        {
            ParamsGame.ScoreCount += Singleton.addScoreStage;
            Singleton.gameUI.UpdateScore(ParamsGame.ScoreCount);
        }

        public static void AddMoney()
        {
            ParamsGame.MoneyCount++;
            Singleton.gameUI.UpdateMoney(ParamsGame.MoneyCount);
        }

        public static void AddKey()
        {
            LoadStatusPlayer.Status.CountKey++;
        }

        public void StartGame()
        {
            mainMenu.CloseMenu();
            CameraControl.Singleton.SetLimit(mapGenerator);
            NextStartGame();
        }

        public void StartGameTutorial()
        {

            player.PlayerSetPosition(mapGenerator.StartGenerateLevelTutorial(), Quaternion.identity);
            CameraControl.Singleton.SetLimit(mapGenerator);
            LoadStatusPlayer.Status.TutorialEnd = true;
            mainMenu.CloseMenu();
            NextStartGame();
        }

        private void NextStartGame()
        {
            player.UnLockPlayer();
            player.UnLockInput();
            gameUI.UpdateMaxScore(LoadStatusPlayer.Status.MaxScore);
            gameUI.UpdateMoney(0);
            gameUI.UpdateScore(0);

            player.PlayerDead += PlayerDead;

            mapGenerator.UpdateNewLocation += AddScore;

            if (CallStartGame != null)
            {
                CallStartGame();
            }
        }

        public void SetCarPlayer(Car carPrefab)
        {
            player.SetCar(carPrefab);
            player.SetLockPlayer();
        }

        private void CallTakeCar(int index)
        {
            SetCarPlayer(carsData.GetCars()[index]);
        }

        private void EndGame()
        {
            Map.KeySpawner.ResetKeys();
            mapGenerator.Clear();

            GenerateStart();
            Pause.IsPause = false;
            mainMenu.OpenMenu();

            player.PlayerDead -= PlayerDead;
            gameUI.EndNoContinue -= EndGame;
            gameUI.Continue -= ContinueGame;
            mapGenerator.UpdateNewLocation -= AddScore;

            if (CallEndGame != null)
            {
                CallEndGame();
            }
            ParamsGame.PushToSave();
        }

        private void GenerateStart()
        {
            player.PlayerSetPosition(mapGenerator.StartGenerateLevel(), Quaternion.identity);
            player.SetLockPlayer();
            player.LockInput();
        }


        private void ContinueGame()
        {
            if (!Advertising.AdvertisingSee.SeeAdvertising())
            {
                EndGame();
                return;
            }

            Vector3 pos = mapGenerator.GetContinuePoint(player.GetPosition());
            if (pos == Vector3.zero)
            {
                pos = player.GetPosition();
            }


            Pause.IsPause = false;
            player.UnLockPlayer();
            gameUI.HideTimerEnd();
            player.ActiveGodMove();
            player.PlayerSetPosition(pos, Quaternion.identity);
            player.Invoke("UnLockInput", 0.3f);

            gameUI.EndNoContinue -= EndGame;
            gameUI.Continue -= ContinueGame;

            if (CallContinueGame != null)
            {
                CallContinueGame();
            }
        }

        private void PlayerDead()
        {
            Pause.IsPause = true;
            player.SetLockPlayer();
            player.LockInput();
            gameUI.EndNoContinue += EndGame;
            gameUI.Continue += ContinueGame;

            if (CallPlayerDead != null)
            {
                CallPlayerDead();
            }
            gameUI.StartTimerEnd(ParamsGame.ScoreCount, ParamsGame.MaxScoreCount, ParamsGame.GetAllMoney());
        }



        public class AcamulatorGameParams
        {
            public int ScoreCount;

            public int MaxScoreCount;

            public int MoneyCount;

            public void PushToSave()
            {
                LoadStatusPlayer.Status.CountMoney += GetAllMoney();
                if (ScoreCount > MaxScoreCount)
                {
                    LoadStatusPlayer.Status.MaxScore = ScoreCount;
                }
                ScoreCount = 0;
                MoneyCount = 0;
                LoadStatusPlayer.SaveStatus();
            }

            public int GetAllMoney()
            {
                return MoneyCount + (ScoreCount / Singleton.addScoreStage);
            }
        }
    }
}