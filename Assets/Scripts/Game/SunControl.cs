using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Base.Game.Map
{
    public class SunControl : MonoBehaviour
    {
        public static event System.Action StartDay;

        public static event System.Action StartNight;

        public static bool IsDay { get; private set; }

        [SerializeField] private Transform light;

        [SerializeField] private float speedTime;

        private float time;

        private void Start()
        {
            return;
            ClearToDay();
            GameProcess.CallEndGame += ClearToDay;
        }



        private void LateUpdate()
        {
            return;
            if (Pause.IsPause) return;

            time += speedTime * Time.timeScale * Time.deltaTime;

            if (time > 3600f)
            {
                time = 0f;
            }

            light.transform.rotation = Quaternion.identity;
            light.Rotate(360f * (time / 3600f), 0f, 0f);

            if (IsDay && time > 900f && time < 2700)
            {
                if (StartNight != null)
                {
                    StartNight();
                }
                IsDay = false;
                
            }

            if (!IsDay && time > 2700)
            {
                if (StartDay != null)
                {
                    StartDay();
                }
                IsDay = true;
            }
        }

        private void ClearToDay() {
            if (StartDay != null)
            {
                StartDay();
            }
            time = 0f;
            IsDay = true;
        }
    }
}