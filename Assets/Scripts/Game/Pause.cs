using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Base
{
    public static class Pause
    {
        public static bool IsPause
        {
            get
            {
                return _isPause;
            }
            set
            {
                if (value)
                {
                    Physics.autoSimulation = false;
                }
                else
                {
                    Physics.autoSimulation = true;
                }

                _isPause = value;
            }
        }

        private static bool _isPause = false;
    }
}