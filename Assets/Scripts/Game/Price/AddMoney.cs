using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Base.UI;

namespace Base.Process.Price
{
    public class AddMoney : Price
    {
        [SerializeField] private int countMoney;

        [SerializeField] private AddMoneyBonusUI prefabUI;

        public override void ToGivePrice()
        {
            LoadStatusPlayer.Status.CountMoney += countMoney;
        }

        public override Transform ShowToUI(Transform parent)
        {
            AddMoneyBonusUI p = Instantiate(prefabUI, parent);
            p.SetCount(countMoney);
            return p.transform;
        }
    }
}