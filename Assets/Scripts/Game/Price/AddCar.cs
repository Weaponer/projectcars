using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Base.Process.Price
{
    public class AddCar : Price
    {
        [SerializeField] private int indexCar;

        [SerializeField] private GameObject uiPrefab;

        public override void ToGivePrice()
        {
            LoadStatusPlayer.Status.AddCar(indexCar);
        }

        public override Transform ShowToUI(Transform parent)
        {
            return Instantiate(uiPrefab, parent).transform;
        }
    }
}