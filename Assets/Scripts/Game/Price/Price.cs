using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Base.Process.Price
{
    public abstract class Price : MonoBehaviour
    {
        public virtual void ToGivePrice() { 
            
        }

        public virtual Transform ShowToUI(Transform parent) {
            return null;
        }
    }
}