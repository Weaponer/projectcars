using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Base.UI;


namespace Base.Process.Price
{
    public class AddKey : Price
    {

        [SerializeField] private AddKeyUI prefabUI;

        public override void ToGivePrice()
        {
            LoadStatusPlayer.Status.CountKey++;
        }

        public override Transform ShowToUI(Transform parent)
        {
            AddKeyUI p = Instantiate(prefabUI, parent);
            return p.transform;
        }
    }
}