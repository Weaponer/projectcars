using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Base.Game;

namespace Base.Process
{
    public class Vibration : MonoBehaviour
    {
        public void InitVibration()
        {
            Player.Singleton.PlayerDead += Vibr;
        }

        private void Vibr()
        {
            if (LoadStatusPlayer.Status.Vibration)
                Handheld.Vibrate();
        }
    }
}