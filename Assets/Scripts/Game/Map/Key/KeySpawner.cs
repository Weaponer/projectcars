using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Base.Process;

namespace Base.Game.Map
{
    public class KeySpawner : MonoBehaviour
    {
        [SerializeField] private int SpawnRandom = 30;

        [SerializeField] private Key keyPrefab;

        [SerializeField] private Transform from;
        [SerializeField] private Transform to;

        private Key key;

        private static event System.Action removeKeys;

        private static int countTake = 0;

        private void Start()
        {
            if (LoadStatusPlayer.Status.CountKey < 3 && countTake < 2)
            {
                if (Random.Range(0, 100) > 30)
                {
                    return;
                }

                Key k = Instantiate(keyPrefab, transform);
                k.transform.position = Vector3.Lerp(from.position, to.position, Random.Range(0.0f, 1.0f));
                k.TakeKey += GameProcess.AddKey;
                k.TakeKey += TK;
                k.TakeKey += TakeKey;
                removeKeys += TK;
                key = k;
            }
        }

        public static void ResetKeys()
        {
            if (removeKeys != null)
                removeKeys();
            countTake = 0;
        }

        private void TK()
        {
            if (key)
            {
                Destroy(key.gameObject);
                key = null;
            }
        }

        private static void TakeKey()
        {
            countTake++;
            if (LoadStatusPlayer.Status.CountKey >= 3 || countTake >= 2)
            {
                if (removeKeys != null)
                {
                    removeKeys();
                }
            }
        }

        private void OnDestroy()
        {
            removeKeys -= TK;
        }
    }
}