using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace Base.Game.Map
{
    public class Key : MonoBehaviour
    {
        public event System.Action TakeKey;

        [SerializeField] private UnityEvent takeKey;

        private void OnTriggerEnter(Collider other)
        {
            if (LayerMask.LayerToName(other.gameObject.layer).IndexOf("CarPlayer") != -1)
            {
                if (TakeKey != null)
                {
                    TakeKey();
                }
                takeKey.Invoke();
            }
        }
    }
}