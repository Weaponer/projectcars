using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Base.Game.Map
{
    public class StartStage : Stage
    {
        [SerializeField] private Transform spawnPos;

        public Vector3 GetPositionSpawn() {
            return spawnPos.position;
        }
    }
}