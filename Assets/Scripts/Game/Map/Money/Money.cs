using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace Base.Game.Map
{
    public class Money : MonoBehaviour
    {
        public event System.Action TakeMoney;

        [SerializeField] private UnityEvent takeKey;

        private void OnTriggerEnter(Collider other)
        {
            if (LayerMask.LayerToName(other.gameObject.layer).IndexOf("CarPlayer") != -1)
            {
                if (TakeMoney != null)
                {
                    TakeMoney();
                }

                takeKey.Invoke();
            }
        }
    }
}