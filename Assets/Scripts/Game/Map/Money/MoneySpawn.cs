using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Base.Game.Map
{
    public class MoneySpawn : MonoBehaviour
    {
        [SerializeField] private int SpawnRandom = 30;

        [SerializeField] private Money moneyPrefab;

        [SerializeField] private Transform from;
        [SerializeField] private Transform to;

        private Money money;

        private void Start()
        {
            if(Random.Range(0, 100) > 30)
            {
                return;
            }

            Money m = Instantiate(moneyPrefab, transform);
            m.transform.position = Vector3.Lerp(from.position, to.position, Random.Range(0.0f,1.0f));
            m.TakeMoney += CallGetMoney;
            money = m;
        }

        private void CallGetMoney() {
            Destroy(money.gameObject);
            GameProcess.AddMoney();
        }
    }
}