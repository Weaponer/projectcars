using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Base.Game.Map
{
    public class StageRoad : Stage
    {
        [SerializeField] private float startMinDistCar = 15f;

        [SerializeField] private float timeDifference = 1.4f;

        [SerializeField] private float timeMoveCar = 3f;

        [SerializeField] private BotCarDataBase botCarDataBase;

        [SerializeField] private Transform fromPoint;

        [SerializeField] private Transform toPoint;

        private List<Car> cars = new List<Car>();


        protected override void StartStage(int num_stage)
        {
            if (Random.Range(0, 3) == 1)
            {
                Transform f = fromPoint;
                fromPoint = toPoint;
                toPoint = f;
            }

            for (int i = 0; i < botCarDataBase.Count; i++)
            {
                if (botCarDataBase.GetBot(i).MinNumStage <= num_stage)
                {
                    cars.Add(botCarDataBase.GetBot(i).BotCar);
                }
            }
            StartCoroutine(updateCar());
        }

        private void CallEnd(MoveObject obj)
        {
            Destroy(obj.gameObject);
        }

        IEnumerator updateCar()
        {
            float startdist = Vector3.Distance(fromPoint.position, toPoint.position);
            float dist = startdist;
            while (true)
            {
                dist -= startMinDistCar + Random.Range(0, startMinDistCar);
                if (dist <= 0)
                {
                    break;
                }

                Car car = SpawnCar();
                car.transform.position = Vector3.Lerp(toPoint.position, fromPoint.position, dist / startdist);
                car.Move(car.transform.position, toPoint.position, timeMoveCar * (dist / startdist));
                car.End += CallEnd;
            }

            yield return new WaitForSeconds(Random.Range(0, timeDifference));
            while (true)
            {
                Car car = SpawnCar();
                car.Move(fromPoint.position, toPoint.position, timeMoveCar);
                car.End += CallEnd;
                yield return new WaitForSeconds(timeDifference + Random.Range(0, timeDifference));
            }
            yield break;
        }

        private Car SpawnCar()
        {
            Car car = Instantiate(cars[Random.Range(0, cars.Count)]);
            car.transform.parent = transform;
            car.transform.localRotation = Quaternion.LookRotation(toPoint.position - fromPoint.position, Vector3.up);

            return car;
        }
    }
}