using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace Base.Game.Map
{
    public class StageTrain : Stage
    {
        public static event System.Action CallPlayerMoveTo;

        [SerializeField] private float timeDifference = 2f;

        [SerializeField] private float timeMoveTrain = 3f;

        [SerializeField] private float timeStartSemafore = 1f;

        [SerializeField] private Train train;

        [SerializeField] private Semaphore[] semaphores;

        [SerializeField] private Transform fromPoint;

        [SerializeField] private Transform toPoint;

        bool startMove;

        [Space(15)]
        [SerializeField] private UnityEvent startSemaforeTrain;
        protected override void StartStage(int num_stage)
        {

            if (Random.Range(0, 3) == 1)
            {
                Transform f = fromPoint;
                fromPoint = toPoint;
                toPoint = f;
            }

            startMove = true;
            train.gameObject.SetActive(false);
            train.End += CallEnd;
            StartCoroutine(updateTrain());

            ContactPlayer += delegate { if (CallPlayerMoveTo != null) CallPlayerMoveTo(); };
        }

        private void CallEnd(MoveObject obj)
        {
            train.gameObject.SetActive(false);
            for (int i = 0; i < semaphores.Length; i++)
            {
                semaphores[i].SemaphoreOff();
            }
            startMove = true;
        }

        IEnumerator updateTrain()
        {
            while (true)
            {
                if (startMove)
                {
                    float time = timeDifference + Random.Range(0, timeDifference);
                    Invoke("StartSemafore", time - timeStartSemafore);
                    yield return new WaitForSeconds(time);
                    train.gameObject.SetActive(true);
                    train.Move(fromPoint.position, toPoint.position, timeMoveTrain);
                    startMove = false;

                }
                yield return null;
            }
        }

        private void StartSemafore()
        {
            for (int i = 0; i < semaphores.Length; i++)
            {
                semaphores[i].SemaphoreOn();
            }
            startSemaforeTrain.Invoke();
        }
    }
}