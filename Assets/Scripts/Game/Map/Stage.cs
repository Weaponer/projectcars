using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Base.Game.Map
{
    public delegate void StageContactPlayer(Stage stage);

    public class Stage : MonoBehaviour
    {
        public event StageContactPlayer ContactPlayer;

        public bool IsContactPlayer { get; private set; }

        [SerializeField] private float widthStage;

        [Space(15)]
        [SerializeField] private GameObject[] randomObjectSpawn;

        public float GetWidth()
        {
            return widthStage;
        }

        public void InitStage(int num_stage)
        {
            for (int i = 0; i < randomObjectSpawn.Length; i++) {
                if (Random.Range(0, 2) == 1) {
                    randomObjectSpawn[i].SetActive(true);
                }
            }
            StartStage(num_stage);
        }

        protected virtual void StartStage(int num_stage) { 
        
        }

        private void OnTriggerEnter(Collider other)
        {
            if (!IsContactPlayer && LayerMask.LayerToName(other.gameObject.layer).IndexOf("CarPlayer") != -1)
            {
                if (ContactPlayer != null)
                {
                    ContactPlayer(this);
                }
                IsContactPlayer = true;
            }
        }

        public void GenerateBorder(Vector2 size)
        {
            GameObject borderObj = new GameObject();
            borderObj.transform.parent = transform;
            borderObj.name = "border";
            borderObj.transform.localPosition = Vector3.zero;
            borderObj.transform.localRotation = Quaternion.identity;
            borderObj.layer = LayerMask.NameToLayer("Border");

            BoxCollider box = borderObj.AddComponent<BoxCollider>();
            box.center = new Vector3(size.x, 10, widthStage / 2f);
            box.size = new Vector3(1, 20, widthStage);

            box = borderObj.AddComponent<BoxCollider>();
            box.center = new Vector3(size.y, 10, widthStage / 2f);
            box.size = new Vector3(1, 20, widthStage);
        }
    }
}
