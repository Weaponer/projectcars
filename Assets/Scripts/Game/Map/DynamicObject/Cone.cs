using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Base.Game.Map
{
    public class Cone : DynamicObject
    {
        public static event System.Action PlayerBumpCone;

        protected override void CallBump()
        {
            if (PlayerBumpCone != null)
            {
                PlayerBumpCone();
            }
        }
    }
}