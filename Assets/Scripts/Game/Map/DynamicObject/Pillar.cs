using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Base.Game.Map
{
    public class Pillar : DynamicObject
    {
        public static event System.Action PlayerBumpPillar;

        protected override void CallBump()
        {
            if (PlayerBumpPillar != null)
            {
                PlayerBumpPillar();
            }
        }
    }
}