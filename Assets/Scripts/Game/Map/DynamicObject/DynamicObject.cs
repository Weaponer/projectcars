using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace Base.Game.Map
{
    public abstract class DynamicObject : MonoBehaviour
    {
        [SerializeField] private Collider[] colliders;

        [SerializeField] private Rigidbody rigidbody;

        [SerializeField] private UnityEvent bumpObject;

        private bool isBump;

        private void OnCollisionEnter(Collision collision)
        {
            if (isBump) return;

            for (int i = 0; i < collision.contactCount; i++)
            {
                GameObject obj = collision.GetContact(i).otherCollider.gameObject;
                
                if (obj.layer == LayerMask.NameToLayer("CarPlayer"))
                {
                    rigidbody.velocity *= 1.8f;
                    isBump = true;
                    CallBump();
                    bumpObject.Invoke();
                    Invoke("CallEditMask", 0.4f);
                    return;
                }
            }
        }

        protected virtual void CallBump()
        {

        }

        private void CallEditMask()
        {
            for (int i2 = 0; i2 < colliders.Length; i2++)
            {
                colliders[i2].gameObject.layer = LayerMask.NameToLayer("BumpObject");
            }
        }
    }
}