using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Base.Game.Map
{
    public class StageStaticWater : StageWater
    {
        [SerializeField] private WaterLilies[] waterLilies;

        [SerializeField] private float minDistSpawn;

        [SerializeField] private Transform fromSpawn;

        [SerializeField] private Transform toSpawn;

        private void Start()
        {
            float startDist = Vector3.Distance(fromSpawn.position, toSpawn.position);
            float dist = startDist;

            while (dist > 0) {
                dist -= minDistSpawn + Random.Range(0, minDistSpawn);
                if (dist <= 0) {
                    break;
                }
                WaterLilies lilies = Instantiate(waterLilies[Random.Range(0, waterLilies.Length)]);
                lilies.transform.parent = transform;
                lilies.transform.position = Vector3.Lerp(fromSpawn.position, toSpawn.position, dist / startDist);
            }
        }
    }
}