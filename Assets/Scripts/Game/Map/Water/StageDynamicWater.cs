using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Base.Game.Map
{
    public class StageDynamicWater : StageWater
    {
        [SerializeField] private float startMinDistBoard = 15f;

        [SerializeField] private float timeDifference = 1.4f;

        [SerializeField] private float timeMoveBoard = 3f;

        [SerializeField] private Board[] boards;

        [SerializeField] private Transform fromPoint;

        [SerializeField] private Transform toPoint;

        private void Start()
        {
            StartCoroutine(updateBoard());
        }

        private void CallEnd(MoveObject obj)
        {
            Destroy(obj.gameObject);
        }

        IEnumerator updateBoard()
        {
            float startdist = Vector3.Distance(fromPoint.position, toPoint.position);
            float dist = startdist;
            while (dist > 0)
            {
                dist -= startMinDistBoard + Random.Range(0, startMinDistBoard);
                if (dist <= 0)
                {
                    break;
                }

                Board board = SpawnBoard();
                board.transform.position = Vector3.Lerp(toPoint.position, fromPoint.position, dist / startdist);
                board.Move(board.transform.position, toPoint.position, timeMoveBoard * (dist / startdist));
                board.End += CallEnd;
            }

            while (true)
            {
                yield return new WaitForSeconds(timeDifference + Random.Range(0, timeDifference));
                Board board = SpawnBoard();
                board.Move(fromPoint.position, toPoint.position, timeMoveBoard);
                board.End += CallEnd;
            }
        }

        private Board SpawnBoard()
        {
            Board board = Instantiate(boards[Random.Range(0, boards.Length)]);
            board.transform.parent = transform;
            board.transform.localRotation = Quaternion.LookRotation(toPoint.position - fromPoint.position, Vector3.up);

            return board;
        }
    }
}
