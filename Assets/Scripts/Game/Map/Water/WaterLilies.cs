using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace Base.Game.Map
{
    public class WaterLilies : MonoBehaviour
    {
        [SerializeField] private Transform moveRenderParts;

        [SerializeField] private float forceMove;

        [SerializeField] private int iterationMove;

        [SerializeField] private UnityEvent moveCarOnLilie;

        private void OnTriggerEnter(Collider other)
        {
            if (other.gameObject.layer == LayerMask.NameToLayer("CarPlayer")) {
                StopAllCoroutines();
                StartCoroutine(move(transform.position, transform.position - new Vector3(0, forceMove, 0)));
            }
        }
        private void OnTriggerExit(Collider other)
        {
            if (other.gameObject.layer == LayerMask.NameToLayer("CarPlayer"))
            {       
                StopAllCoroutines();
                moveCarOnLilie.Invoke();
                StartCoroutine(move(transform.position - new Vector3(0, forceMove, 0), transform.position));
            }
        }

        IEnumerator move(Vector3 from, Vector3 to) {
            for (int i = 1; i < iterationMove; i++) {
                moveRenderParts.position = Vector3.Lerp(from, to, (float)i / iterationMove);
                yield return new WaitForSeconds(0.01f);
            }
            yield break;
        }
    }
}