using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Base.Game.Map
{
    public class Semaphore : MonoBehaviour
    {
        [SerializeField] private GameObject parentLight;

        [SerializeField] private Light lightUp;
        [SerializeField] private Light lightDown;


        [SerializeField] private float speedClk; 

        private bool isOn;
      
        public void SemaphoreOn() {
            isOn = true;
        }

        public void SemaphoreOff() {
            isOn = false;
        }

        private void OnEnable()
        {
            parentLight.SetActive(true);
            StartCoroutine(clkLight()); ;
        }

        private void OnDisable()
        {
            parentLight.SetActive(false);
            StopAllCoroutines();
        }

        IEnumerator clkLight() {
            while (true) {
                if (!Pause.IsPause)
                {
                    if (!isOn)
                    {
                        lightUp.enabled = false;
                        lightDown.enabled = false;
                    }
                    else
                    {
                        lightDown.enabled = !lightDown.enabled;
                        lightUp.enabled = !lightDown.enabled;
                    }
                }
                yield return new WaitForSeconds(speedClk);
            }
        }
    }
}