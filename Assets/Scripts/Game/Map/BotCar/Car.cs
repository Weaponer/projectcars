using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Base.Game.Map
{
    public class Car : MoveObject
    {
        [SerializeField] private Transform[] wheels;

        [SerializeField] private float wheelRadius;

        [SerializeField] private GameObject lightObject;

        bool isStart;

        float angAll;

        protected override void CallStart()
        {
            isStart = true;

            return;
            if (!SunControl.IsDay)
            {
                OnLight();
            }    
            Map.SunControl.StartDay += OffLight;
            Map.SunControl.StartNight += OnLight;
        }

        private void Update()
        {
            if (isStart && !Pause.IsPause)
            {
                float speed = GetSpeed();
                float dist = speed * Time.deltaTime * Time.timeScale;

                float distWheel = 2 * wheelRadius * Mathf.PI;

                float ang = 360 * (dist / distWheel);
                angAll += ang;

                for (int i = 0; i < wheels.Length; i++)
                {
                    wheels[i].Rotate(Vector3.right, angAll, Space.Self);
                }
            }
        }

        protected override void CallEnd()
        {
            Map.SunControl.StartDay -= OffLight;
            Map.SunControl.StartNight -= OnLight;
        }

        private void OffLight()
        {
            if (lightObject)
                lightObject.SetActive(false);
        }

        private void OnLight()
        {
            if (lightObject)
                lightObject.SetActive(true);
        }
    }
}