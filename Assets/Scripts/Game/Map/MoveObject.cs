using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Base.Game.Map
{
    public delegate void EndMoveObject(MoveObject moveObject);

    public class MoveObject : MonoBehaviour
    {
        public event EndMoveObject End;

        private Coroutine coroutine;

        private float speed;

        public void Move(Vector3 from, Vector3 to, float time)
        {
            RemoveCoroutine();
            coroutine = StartCoroutine(MoveCur(from, to, time));
            speed = Vector3.Distance(from, to) / time;
        }

        public float GetSpeed()
        {
            return speed;
        }

        protected virtual void CallStart()
        {

        }

        protected virtual void CallEnd()
        {

        }

        private void RemoveCoroutine()
        {
            if (coroutine != null)
            {
                StopCoroutine(coroutine);
                coroutine = null;
            }
        }

        IEnumerator MoveCur(Vector3 from, Vector3 to, float time)
        {
            CallStart();
            Transform tr = gameObject.transform;
            tr.position = from;

            float dist = Vector3.Distance(from, to);
            float speed = dist / time;
            Vector3 normal = (to - from).normalized;
            while (dist > speed)
            {
                dist = Vector3.Distance(transform.position, to);
                transform.position += normal * Mathf.Min(dist, speed * Time.timeScale * Time.deltaTime);
                yield return new WaitForEndOfFrame();
            }

            /*  int countIt = (int)(time / 0.008f);
              float wait = time / countIt;
              for (int i = 1; i <= countIt; i++)
              {
                  tr.position = Vector3.Lerp(from, to, (float)i / countIt);
                  yield return new WaitForSeconds(0.008f);
                  if (Pause.IsPause)
                  {
                      i--;
                  }
              }*/

            tr.position = to;
            if (End != null)
            {
                End(this);
            }
            CallEnd();
            RemoveCoroutine();
            speed = 0;
            yield break;
        }
    }
}