using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Base.Game.Map
{
    public class MapGenerator : MonoBehaviour
    {
        public event System.Action UpdateNewLocation;
        [Header("Random params")]
        [Range(0, 10)]
        [SerializeField] private int randomSpawnWater = 4;

        [Range(0, 10)]
        [SerializeField] private int randomSpawnTrain = 3;

        [Range(0, 10)]
        [SerializeField] private int randomSpawnGromFromWater = 4;

        [Range(0, 10)]
        [SerializeField] private int randomSpawnGromFromTrain = 2;

        [Range(0, 10)]
        [SerializeField] private int randomSpawnGromFromRoad = 6;

        [Space(15)]
        [Header("Locations")]
        [SerializeField] private StartStage tutorialPart;

        [SerializeField] private StartStage startPart;

        [SerializeField] private StageRoad[] roadParts;

        [SerializeField] private StageGround[] groundParts;

        [SerializeField] private StageWater[] waterParts;

        [SerializeField] private StageTrain[] trainParts;

        [Space(15)]
        [Header("Params")]
        [SerializeField] private Vector2 limit;

        [SerializeField] private float distCameraToBackBorder;

        [SerializeField] private int limitCountStage = 15;

        [SerializeField] private int startSpawnStage = 6;


        private List<Stage> stages = new List<Stage>();

        private Vector3 nextPos;

        private float backLimit;

        private GameObject borderBackObj;

        private BoxCollider borderBack;

        private Stage oldStage;

        private int countStage;

        public Vector2 GetLimit()
        {
            return new Vector2(limit.x + transform.position.x, limit.y + transform.position.x); ;
        }

        public float GetBackLimitCamera()
        {
            return backLimit;
        }

        public void Clear()
        {
            for (int i = 0; i < stages.Count; i++)
            {
                Destroy(stages[i].gameObject);
            }
            stages.Clear();

            Destroy(borderBackObj);
            borderBack = null;
            borderBackObj = null;
        }

        public Vector3 StartGenerateLevel()
        { // return spawn player
            nextPos = Vector3.zero;
            Stage st = CreateLocation(startPart);
            countStage = 0;
            st.InitStage(countStage);

            NextStartGenerateLevel();
            return startPart.GetPositionSpawn();
        }

        public Vector3 StartGenerateLevelTutorial()
        {
            nextPos = Vector3.zero;
            Stage st = CreateLocation(tutorialPart);
            countStage = 0;
            st.InitStage(countStage);

            NextStartGenerateLevel();
            return tutorialPart.GetPositionSpawn();
        }

        public Vector3 GetContinuePoint(Vector3 carPos)
        {
            Stage st = null;
            for (int i = 2; i < stages.Count; i++)
            {
                if (!stages[i].IsContactPlayer)
                {
                    st = stages[i];
                    break;
                }
            }

            bool isTp = true;
            for (int i = 0; i < stages.Count; i++)
            {
                if (stages[i].GetComponent<StageWater>() != null && ((carPos.z - stages[i].transform.position.z) * (carPos.z - stages[i].transform.position.z)) < st.GetWidth() * st.GetWidth() * 2)
                {
                    isTp = false;
                    break;
                }
            }

            if (isTp)
            {
                return Vector3.zero;
            }

            for (int i = startSpawnStage / 2; i < stages.Count; i++)
            {
                if (stages[i].GetComponent<StageWater>() == null)
                {
                    return stages[i].transform.position + (stages[i].transform.forward * stages[i].GetWidth() / 2f);
                }
            }
            return Vector3.zero;
        }

        private void NextStartGenerateLevel()
        {
            for (int i = 0; i < startSpawnStage; i++)
            {
                Stage st = CreateLocation(roadParts[Random.Range(0, roadParts.Length)]);
                st.ContactPlayer += (x) => { if (UpdateNewLocation != null) UpdateNewLocation(); };
                st.ContactPlayer += UpdateContactPlayer;
            }


            borderBackObj = new GameObject();
            borderBackObj.transform.parent = transform;
            borderBackObj.layer = LayerMask.NameToLayer("BorderEnemy");
            borderBackObj.name = "Enemy Border";
            borderBackObj.transform.localPosition = Vector3.zero;
            borderBackObj.transform.localRotation = Quaternion.identity;

            borderBack = borderBackObj.AddComponent<BoxCollider>();
            borderBack.size = new Vector3(Mathf.Abs(limit.x) + Mathf.Abs(limit.y), 20, 1);
            UpdateBackBorder();
        }

        private void UpdateContactPlayer(Stage stage)
        {
            Stage st = GetRandomStage();
            st = CreateLocation(st);
            st.ContactPlayer += (x) => { if (UpdateNewLocation != null) UpdateNewLocation(); };
            st.ContactPlayer += UpdateContactPlayer;

            if (stages.Count > limitCountStage)
            {
                RemoveLocation(stages[0]);
                UpdateBackBorder();
            }
        }

        private void UpdateBackBorder()
        {
            borderBack.center = new Vector3((limit.x + limit.y) / 2, 10, borderBackObj.transform.InverseTransformPoint(stages[0].transform.position).z);
            backLimit = stages[0].transform.position.z + distCameraToBackBorder;
        }

        private Stage CreateLocation(Stage stage)
        {
            Stage st = Instantiate(stage, nextPos, Quaternion.identity);
            nextPos.z += st.GetWidth();
            stages.Add(st);
            st.GenerateBorder(limit);
            oldStage = stage;

            countStage++;
            st.InitStage(countStage);
            return st;
        }

        private void RemoveLocation(Stage stage)
        {
            stages.Remove(stage);
            Destroy(stage.gameObject);
        }

        private Stage GetRandomStage()
        {
            if (oldStage.GetComponent<StageGround>() || oldStage.GetComponent<StartStage>())
            {
                if (Random.Range(0, randomSpawnWater) == 1)
                {
                    return waterParts[Random.Range(0, waterParts.Length)];
                }
                else if (Random.Range(0, randomSpawnTrain) == 1)
                {
                    return trainParts[Random.Range(0, trainParts.Length)];
                }
                else
                {
                    return roadParts[Random.Range(0, roadParts.Length)];
                }
            }

            if (oldStage.GetComponent<StageTrain>() && Random.Range(0, randomSpawnGromFromTrain) == 1)
            {
                return groundParts[Random.Range(0, groundParts.Length)];
            }
            else if (oldStage.GetComponent<StageRoad>() && Random.Range(0, randomSpawnGromFromRoad) == 1)
            {
                return groundParts[Random.Range(0, groundParts.Length)];
            }
            else if (oldStage.GetComponent<StageWater>() && Random.Range(0, randomSpawnGromFromWater) >= 1)
            {
                return groundParts[Random.Range(0, groundParts.Length)];
            }
            else
            {
                return oldStage;
            }
        }
    }
}