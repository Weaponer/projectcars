using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Base.Game
{
    public class CameraControl : MonoBehaviour
    {
        public static CameraControl Singleton;

        public static Camera CameraGame;

        [SerializeField] private Camera camera;


        private Car car;

        private Vector3 oldVelocity = Vector3.zero;

        private Map.MapGenerator generator;

        private void Awake()
        {
            if (Singleton)
            {
                Destroy(gameObject);
                return;
            }
            else
            {
                Singleton = this;
                CameraGame = camera;
            }
        }

        private void OnDestroy()
        {
            if (Singleton == this)
            {
                Singleton = null;
                CameraGame = null;
            }
        }

        public void SetCameraToCar(Car car)
        {
            this.car = car;
            transform.position = car.transform.position;
        }

        public void SetLimit(Map.MapGenerator mapGenerator)
        {
            generator = mapGenerator;
        }

        private void FixedUpdate()
        {
            if (!car) return;

            transform.position += (car.transform.position - transform.position) * 4f * Time.fixedDeltaTime;

            if (generator != null)
            {
                Vector3 pos = transform.position;
                pos.z = Mathf.Clamp(pos.z, generator.GetBackLimitCamera(), float.MaxValue);
                pos.y = 0;
                transform.position = pos;
            }
        }
    }
}