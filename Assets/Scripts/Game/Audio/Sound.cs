using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Base.Process;

namespace Base.Game
{
    public class Sound : MonoBehaviour
    {
        [SerializeField] private AudioSource[] clips;

        public void StartClips()
        {
            if (AudioControl.IsActiveSound())
            {
                for (int i = 0; i < clips.Length; i++)
                {
                    clips[i].Play();
                }
            }
        }
    }
}