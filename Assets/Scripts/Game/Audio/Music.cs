using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Base.Process;

namespace Base.Game
{
    public class Music : MonoBehaviour
    {
        [SerializeField] private AudioSource[] clips;

        private void OnEnable()
        {
            if (AudioControl.IsActiveMusic()) {
                StartM();
            }
            AudioControl.StartMusic += StartM;
            AudioControl.StopMusic += StopM;
        }

        private void OnDisable()
        {
            StopM();
            AudioControl.StartMusic -= StartM;
            AudioControl.StopMusic -= StopM;
        }

        private void StartM() {
            for (int i = 0; i < clips.Length; i++) {
                clips[i].Play();
            }
        }

        private void StopM() {
            for (int i = 0; i < clips.Length; i++)
            {
                clips[i].Stop();
            }
        }
    }
}