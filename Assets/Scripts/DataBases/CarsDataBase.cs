using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Base.Game;
using System.Linq;

namespace Base
{
    [CreateAssetMenu(fileName = "Cars data base", menuName = "DataBase/CarsDataBase")]
    public class CarsDataBase : ScriptableObject
    {
        [SerializeField] private CarPlayerItem[] cars;

        public Car[] GetCars()
        {
            return cars.Select((x) => (x.PlayerCar)).ToArray();

        }

        [System.Serializable]
        public class CarPlayerItem
        {
            public int Const;

            public Car PlayerCar;
        }
    }
}