using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Base
{
    [CreateAssetMenu(fileName = "Bot cars data base", menuName = "DataBase/BotCarsDataBase")]
    public class BotCarDataBase : ScriptableObject
    {
        public int Count => botCars.Length;

        [SerializeField] private ItemBotCar[] botCars;

        public ItemBotCar GetBot(int index)
        {
            return botCars[index];
        }

        [System.Serializable]
        public class ItemBotCar
        {
            public Game.Map.Car BotCar;
            public int MinNumStage;
        }
    }
}