using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Base.Process;
using System;

namespace Base
{
    [CreateAssetMenu(fileName = "Tasks data base", menuName = "DataBase/TasksDataBase")]
    public class TaskDataBase : ScriptableObject
    {
        [SerializeField] private TaskObject[] taskObjectsEasy;

        [SerializeField] private TaskObject[] taskObjectsHard;

        public TaskObject GetTastWithData(TaskData T)
        {
            List<TaskObject> taskObjects = new List<TaskObject>();
            taskObjects.AddRange(taskObjectsEasy);
            taskObjects.AddRange(taskObjectsHard);

            for (int i = 0; i < taskObjects.Count; i++)
            {
                if (taskObjects[i].IsCurrentTypeDate(T))
                {
                    return taskObjects[i];
                }
            }
            return null;
        }

        public T GetTast<T>() where T : TaskObject
        {
            List<TaskObject> taskObjects = new List<TaskObject>();
            taskObjects.AddRange(taskObjectsEasy);
            taskObjects.AddRange(taskObjectsHard);

            for (int i = 0; i < taskObjects.Count; i++)
            {
                if (typeof(T) == taskObjects[i].GetType())
                {
                    return (T)taskObjects[i];
                }
            }
            return null;
        }


        public TaskObject[] GetRandomTask(int countEasy, int countHard)
        {
            TaskObject[] tasks = new TaskObject[countEasy + countHard];
            for (int i = 0; i < countEasy; i++)
            {
                TaskObject task = taskObjectsEasy[UnityEngine.Random.Range(0, taskObjectsEasy.Length)];
                tasks[i] = task;
                for (int i2 = 0; i2 < tasks.Length; i2++)
                {
                    if (tasks[i2] == task && i != i2)
                    {
                        tasks[i] = null;
                        i--;
                        break;
                    }
                }
            }

            for (int i = countEasy; i < countEasy + countHard; i++)
            {
                TaskObject task = taskObjectsHard[UnityEngine.Random.Range(0, taskObjectsHard.Length)];
                tasks[i] = task;
                for (int i2 = 0; i2 < tasks.Length; i2++)
                {
                    if (tasks[i2] == task && i != i2)
                    {
                        tasks[i] = null;
                        i--;
                        break;
                    }
                }
            }

            return tasks;
        }
    }
}