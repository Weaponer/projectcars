using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Base.Process.Price;

namespace Base
{
    [CreateAssetMenu(fileName = "Prices data base", menuName = "DataBase/PricesDataBase")]
    public class DailyPriceDataBase : ScriptableObject
    {
        [SerializeField] private Price[] prices;

        public int Count => prices.Length;

        public Price GetPrice(int index) {
            return prices[index];
        }
    }
}