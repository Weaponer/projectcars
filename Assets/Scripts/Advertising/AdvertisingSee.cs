using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Base.Advertising
{
    public static class AdvertisingSee
    {
        public static readonly string IDProgram = "";

        public static bool SeeAdvertising() {
            return true;
        }

        public static void OpenPlayMarket() {
#if UNITY_EDITOR
#else
            Application.OpenURL("market://details?id=" + IDProgram);
#endif
        }
    }
}