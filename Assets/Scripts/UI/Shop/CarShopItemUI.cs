using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Base.UI
{
    public delegate void TakeCar(int index);

    public class CarShopItemUI : MonoBehaviour
    {
        public int IndexCar
        {
            get
            {
                return indexCarInDataBase;
            }
        }

        public event TakeCar CallTakeCar;

        [SerializeField] private int indexCarInDataBase;

        [SerializeField] private Button clickItem;

        [SerializeField] private Image lockImage;

        public void InitItem() {
            clickItem.onClick.AddListener(CallClk);
        }

        private void CallClk() {
            if (CallTakeCar != null) {
                CallTakeCar(IndexCar);
            }
        }

        public void SetLock() {
            clickItem.interactable = false;
            lockImage.gameObject.SetActive(true);
        }

        public void UnLock() {
            clickItem.interactable = true;
            lockImage.gameObject.SetActive(false);
        }
    }
}