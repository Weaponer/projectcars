using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Base.Process;

namespace Base.UI
{
    public delegate void TakePlayerCar(int index);

    public class ShopPanelUI : PanelUI
    {
        public static event TakePlayerCar CarTake;

        public static event System.Action BuyInShop;



        [SerializeField] private int costCarM;

        [Space(15)]
        [SerializeField] private CarsDataBase carsData;

        [SerializeField] private MainMenu mainMenu;

        [SerializeField] private Button close;

        [SerializeField] private CarShopItemUI[] orgeCars;

        [SerializeField] private CarShopItemUI[] blueCars;

        [SerializeField] private CarShopItemUI[] purpleCars;

        [SerializeField] private Button openBlue;

        [SerializeField] private Button openOrge;

        [SerializeField] private Button openPurple;

        [SerializeField] private Text countMoneyText;

        private void Start()
        {
            UpdateCost();

            openBlue.onClick.AddListener(BuyVideoCar);
            openOrge.onClick.AddListener(BuyMoneyCar);
            openPurple.onClick.AddListener(BuyFiveRateCar);

            mainMenu.ShopCarsClk += delegate { mainMenu.OpenPanel(this); };
            close.onClick.AddListener(delegate { mainMenu.ClosePanel(); });

            for (int i = 0; i < orgeCars.Length; i++)
            {
                orgeCars[i].InitItem();
                orgeCars[i].CallTakeCar += CallTakeCar;
            }
            for (int i = 0; i < blueCars.Length; i++)
            {
                blueCars[i].InitItem();
                blueCars[i].CallTakeCar += CallTakeCar;
            }
            for (int i = 0; i < purpleCars.Length; i++)
            {
                purpleCars[i].InitItem();
                purpleCars[i].CallTakeCar += CallTakeCar;
            }
        }

        protected override void CallOpen()
        {
            UpdateLockItem(orgeCars);
            UpdateLockItem(blueCars);
            UpdateLockItem(purpleCars);
            SetActiveButtonNormal(blueCars, openBlue);
            SetActiveButtonNormal(orgeCars, openOrge);
            SetActivePurpleButton();
        }

        protected override void CallClose()
        {
            LoadStatusPlayer.SaveStatus();
        }

        private void CallTakeCar(int car)
        {
            LoadStatusPlayer.Status.TakeCar = car;
            if (CarTake != null)
            {
                CarTake(car);
            }
        }

        private void CallShopEvent()
        {
            if (BuyInShop != null)
            {
                BuyInShop();
            }
        }

        private void BuyMoneyCar()
        {
            if (costCarM * LoadStatusPlayer.Status.shopIndexMultiply <= LoadStatusPlayer.Status.CountMoney)
            {
                LoadStatusPlayer.Status.CountMoney -= costCarM * LoadStatusPlayer.Status.shopIndexMultiply;
                LoadStatusPlayer.Status.shopIndexMultiply++;
                UpdateCost();
                OpenRandomCar(orgeCars);
                SetActiveButtonNormal(orgeCars, openOrge);
                CallShopEvent();
            }
        }

        private void BuyVideoCar()
        {
            if (Advertising.AdvertisingSee.SeeAdvertising())
            {
                OpenRandomCar(blueCars);
                SetActiveButtonNormal(blueCars, openBlue);
                CallShopEvent();
            }
        }

        private void BuyFiveRateCar()
        {
            LoadStatusPlayer.Status.IsSetFiveRate = true;
            OpenRandomCar(purpleCars);
            SetActivePurpleButton();
            CallShopEvent();
            //Advertising.AdvertisingSee.OpenPlayMarket();
        }

        private void UpdateCost()
        {
            countMoneyText.text = (costCarM * LoadStatusPlayer.Status.shopIndexMultiply).ToString();
        }

        private void OpenRandomCar(CarShopItemUI[] carArr)
        {
            List<CarShopItemUI> noOpen = new List<CarShopItemUI>();
            for (int i = 0; i < carArr.Length; i++) {
                if (LoadStatusPlayer.Status.NumOpenCars.IndexOf(carArr[i].IndexCar) == -1) {
                    noOpen.Add(carArr[i]);
                }
            }

            if(noOpen.Count > 0)
            {
                int num = Random.Range(0, noOpen.Count);
                if (LoadStatusPlayer.Status.NumOpenCars.IndexOf(noOpen[num].IndexCar) == -1)
                {
                    LoadStatusPlayer.Status.NumOpenCars.Add(noOpen[num].IndexCar);
                    noOpen[num].UnLock();
                    LoadStatusPlayer.Status.TakeCar = noOpen[num].IndexCar;
                    CallTakeCar(carArr[num].IndexCar);
                }
            }
        }

        private void SetActiveButtonNormal(CarShopItemUI[] carArr, Button btt)
        {
            List<int> openCar = LoadStatusPlayer.Status.NumOpenCars;
            int count = 0;
            for (int i = 0; i < carArr.Length; i++)
            {

                if (openCar.IndexOf(carArr[i].IndexCar) != -1)
                {
                    count++;
                }

            }
            if (count == carArr.Length)
                btt.interactable = false;
            else
                btt.interactable = true;
        }

        private void SetActivePurpleButton()
        {
            if (LoadStatusPlayer.Status.IsSetFiveRate)
            {
                openPurple.interactable = false;
            }
        }

        private void UpdateLockItem(CarShopItemUI[] carArr)
        {
            List<int> openCar = LoadStatusPlayer.Status.NumOpenCars;
            for (int i = 0; i < carArr.Length; i++)
            {
                carArr[i].SetLock();
                if (openCar.IndexOf(carArr[i].IndexCar) != -1)
                {
                    carArr[i].UnLock();
                }

            }
        }
    }
}