using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace Base.UI
{
    public class MovePanelUI : MonoBehaviour
    {
        [SerializeField] private Transform one;
        [SerializeField] private Transform two;

        [SerializeField] private float time;


        Coroutine coroutine;

        public void MoveOn()
        {
            RemoveCoroutine();
            coroutine = StartCoroutine(MoveCur(two.position, one.position, time));
        }

        public void MoveOff()
        {
            RemoveCoroutine();
            coroutine = StartCoroutine(MoveCur(one.position, two.position, time));
        }

        private void RemoveCoroutine()
        {
            if (coroutine != null)
            {
                StopCoroutine(coroutine);
                coroutine = null;
            }
        }

        IEnumerator MoveCur(Vector3 from, Vector3 to, float time)
        {
            Transform tr = gameObject.transform;
            tr.position = from;

            int countIt = (int)(time / 0.01f);
            float wait = time / countIt;
            for (int i = 1; i <= countIt; i++)
            {
                tr.position = Vector3.Lerp(from, to, (float)i / countIt);
                yield return new WaitForSecondsRealtime(wait);
            }

            tr.position = to;
            RemoveCoroutine();
            yield break;
        }
    }
}