using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Base.UI
{
    public class PanelUI : MonoBehaviour
    {
        [SerializeField] private GameObject panel;

        private void Awake()
        {
            panel.SetActive(false);
        }

        public void Open()
        {
            panel.SetActive(true);
            CallOpen();
        }

        protected virtual void CallOpen() { }

        public void Close() {
            panel.SetActive(false);
            CallClose();
        }

        protected virtual void CallClose() { }
    }
}