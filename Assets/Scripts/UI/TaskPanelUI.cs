using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Base.Process;
using Base.Process.Task;

namespace Base.UI
{
    public class TaskPanelUI : PanelUI
    {
        [Space(15)]
        [SerializeField] private MainMenu mainMenu;

        [SerializeField] private Button closeButton;
        [SerializeField] private Transform collectionTask;

        private List<TaskUI> taskUIlist = new List<TaskUI>();

        private void Start()
        {
            mainMenu.TaskGamePanelClk += delegate { mainMenu.OpenPanel(this); };

            closeButton.onClick.AddListener(ClkClose);
        }

        protected override void CallOpen()
        {
            TaskObject[] taskObject = TasksControl.TaskListData;
            for (int i = 0; i < taskObject.Length; i++) {
               taskUIlist.Add(taskObject[i].ShowTask(collectionTask));
            }
        }

        protected override void CallClose()
        {
            for (int i = 0; i < taskUIlist.Count; i++) {
                Destroy(taskUIlist[i].gameObject);
            }
            taskUIlist.Clear();
        }

        private void ClkClose()
        {
            mainMenu.ClosePanel();
        }
    }
}