using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Base.Process;

namespace Base.UI
{
    public class OptionPanel : PanelUI
    {
        [Space(15)]
        [SerializeField] private MainMenu mainMenu;

        [SerializeField] private Button okButton;
        [SerializeField] private Button closeButton;
        [SerializeField] private Toggle music;
        [SerializeField] private Toggle sound;
        [SerializeField] private Toggle vibration;

        private void Start()
        {
            mainMenu.OptionClk += delegate { mainMenu.OpenPanel(this); };
            music.onValueChanged.AddListener(delegate { AudioControl.SetActiveMusic(music.isOn); });
            sound.onValueChanged.AddListener(delegate { AudioControl.SetActiveSound(sound.isOn); });
            vibration.onValueChanged.AddListener(delegate { LoadStatusPlayer.Status.Vibration = vibration.isOn; });

            okButton.onClick.AddListener(ClkOk);
            closeButton.onClick.AddListener(ClkOk);
        }

        protected override void CallOpen()
        {
            music.isOn = LoadStatusPlayer.Status.Music;
            sound.isOn = LoadStatusPlayer.Status.Sound;
            vibration.isOn = LoadStatusPlayer.Status.Vibration;
        }

        protected override void CallClose()
        {

        }

        private void ClkOk()
        {
            LoadStatusPlayer.SaveStatus();
            mainMenu.ClosePanel();
        }
    }
}