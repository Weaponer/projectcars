using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Base.UI
{
    public delegate void ClkDayUI(DayItemUI dayItemUI);

    public class DayItemUI : MonoBehaviour
    {
        public event ClkDayUI Clk;

        public Transform SetUITrans
        {
            get
            {
                return setUI;
            }
        }

        public int NumDay
        {
            get
            {
                return numDay;
            }
        }

        [SerializeField] private Button clkButton;

        [SerializeField] private Image lockImage;

        [SerializeField] private Transform setUI;

        [SerializeField] private Text dayText;

        private int numDay;

        public void InitItem(int numDay)
        {
            clkButton.onClick.AddListener(delegate { if (Clk != null) Clk(this); });
            this.numDay = numDay;
            dayText.text = numDay + "Day";
        }

        public void SetLock()
        {
            lockImage.gameObject.SetActive(true);
            clkButton.interactable = false;
        }

        public void UnLock()
        {
            lockImage.gameObject.SetActive(false);
            clkButton.interactable = true;
        }

    }
}