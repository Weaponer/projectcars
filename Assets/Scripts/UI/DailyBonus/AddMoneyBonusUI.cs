using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


namespace Base.UI
{
    public class AddMoneyBonusUI : MonoBehaviour
    {
        [SerializeField] private Text text;

        public void SetCount(int count)
        {
            if (text != null)
                text.text = count.ToString();
        }
    }
}