using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Base.Process;
using System;

namespace Base.UI
{
    public class DailyBonusPanelUI : PanelUI
    {
        [SerializeField] private Button close;

        [SerializeField] private MainMenu mainMenu;

        [SerializeField] private DailyPriceDataBase dailyPriceData;

        [SerializeField] private DayItemUI[] dayItems;

        [SerializeField] private Transform content;

        private void Start()
        {
            mainMenu.DailyBonusClk += delegate { mainMenu.OpenPanel(this); };
            close.onClick.AddListener(delegate { mainMenu.ClosePanel(); });

            DailuBonusList dailu = LoadStatusPlayer.Status.Dailu;
            int distDay = getCountDistDay(dailu.Year, dailu.Month, dailu.Day);
            for (int i = 0; i < 7; i++)
            {
                DayItemUI day = dayItems[i];
                day.InitItem(i + 1);
                day.SetLock();


                if (dailu.CounOpen > i)
                {
                    day.UnLock();
                    dailyPriceData.GetPrice(dailu.listPrice[i]).ShowToUI(day.SetUITrans);
                }
                else if (i <= distDay)
                {
                    day.UnLock();
                    day.Clk += ClkOpen;
                }
            }
        }

        private void ClkOpen(DayItemUI day)
        {
            dailyPriceData.GetPrice(LoadStatusPlayer.Status.Dailu.listPrice[day.NumDay]).ShowToUI(day.SetUITrans);
            day.Clk -= ClkOpen;
            dailyPriceData.GetPrice(LoadStatusPlayer.Status.Dailu.listPrice[day.NumDay]).ToGivePrice();
            LoadStatusPlayer.Status.Dailu.CounOpen++;
            LoadStatusPlayer.SaveStatus();
        }

        protected override void CallOpen()
        {

        }

        protected override void CallClose()
        {

        }

        private int getCountDistDay(int year, int month, int day)
        {
            DateTime oldTime = new DateTime(year, month, day);
            DateTime time = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day);
            return (time - oldTime).Days;
        }
    }
}