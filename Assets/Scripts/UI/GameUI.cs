using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Base.Process.Task;
using Base.Process;


namespace Base.UI
{
    public class GameUI : MonoBehaviour
    {
        public event System.Action EndNoContinue;

        public event System.Action Continue;

        [SerializeField] private GameObject basePanel;

        [Space(15)]
        [SerializeField] private Transform pointTask;

        [Space(15)]
        [SerializeField] private Text score;

        [SerializeField] private Text scoreMax;

        [SerializeField] private Text money;

        //Result
        [Space(15)]
        [SerializeField] private GameObject panel;

        [SerializeField] private Text resultScore;

        [SerializeField] private Text resultMaxScore;

        [SerializeField] private Text resultMoney;

        [SerializeField] private Button continueButton;

        [SerializeField] private Text continueTimer;


        private Coroutine endTimer;

        private void Awake()
        {
            UIOff();

            continueButton.onClick.AddListener(delegate { ClkButtonContinue(); });
        }

        public void UpdateMoney(int count)
        {
            money.text = "Money: " + count;
        }

        public void UpdateScore(int score)
        {
            this.score.text = "Score: " + score;
        }

        public void UpdateMaxScore(int score)
        {
            this.scoreMax.text = "Max score: " + score;
        }

        public void UIOff()
        {
            RemoveCorotine();
            basePanel.SetActive(false);
        }

        public void UIOn()
        {
            RemoveCorotine();
            basePanel.SetActive(true);
        }

        public void StartTimerEnd(int score, int maxScore, int money)
        {
            RemoveCorotine();
            panel.SetActive(true);
            endTimer = StartCoroutine(TimerEnd(score, maxScore, money));
        }

        IEnumerator TimerEnd(int score, int maxScore, int money)
        {
            resultScore.text = score + " m";
            resultMaxScore.text = maxScore + " m";
            resultMoney.text = money.ToString();

            for (int i = 0; i < pointTask.childCount; i++) {
                Destroy(pointTask.GetChild(0).gameObject);
            }

            for (int i = 0; i < TasksControl.TaskListData.Length; i++) {
                TaskObject task = TasksControl.TaskListData[i];
                if(task.IsShowTask()) {
                    task.ShowTask(pointTask);
                }
            }

            for (int i = 10; i > 0; i--)
            {
                continueTimer.text = i.ToString();
                yield return new WaitForSecondsRealtime(1);         
            }

            if (EndNoContinue != null) {
                EndNoContinue();
            }

            RemoveCorotine();
        }


        public void HideTimerEnd() {
            RemoveCorotine();
        }

        private void ClkButtonContinue() {
            RemoveCorotine();
            if (Continue != null)
            {
                Continue();
            }
        }

        private void RemoveCorotine()
        {
            if (endTimer != null)
            {
                StopCoroutine(endTimer);
                endTimer = null;
                panel.SetActive(false);
            }
        }
    }
}