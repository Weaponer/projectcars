using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Base.Game;
using Base.Advertising;
using Base.Process;
using Base.Process.Price;

namespace Base.UI
{
    public class RoulettePanelUI : PanelUI
    {
        public static event System.Action CallPlay;

        [SerializeField] private MainMenu mainMenu;

        [SerializeField] private Button close;

        [SerializeField] private Button startMoveMoney;

        [SerializeField] private Button startMoveVideo;

        [Space(10)]
        [SerializeField] private HorizontalLayoutGroup leftPoint;

        [SerializeField] private Transform prefabItem;

        [Space(10)]
        [SerializeField] private int costRoulette;

        [Space(10)]
        [SerializeField] private int countItem;

        [SerializeField] private int numStopItem;

        [SerializeField] private int baseOffest;

        [SerializeField] private int speedMove;

        [Space(15)]
        [SerializeField] private ItemPrice[] prices;

        private List<Transform> items = new List<Transform>();

        private List<ItemPrice> pricesSpawn = new List<ItemPrice>();

        private void Start()
        {
            mainMenu.RouletteClk += delegate { mainMenu.OpenPanel(this); };
            close.onClick.AddListener(delegate { mainMenu.ClosePanel(); });
            startMoveMoney.onClick.AddListener(delegate { ClkStartMoney(); });
            startMoveVideo.onClick.AddListener(delegate { ClkStartVideo(); });
        }

        protected override void CallOpen()
        {
            ResetRoulette();
            UpdateButton();
        }

        protected override void CallClose()
        {

        }

        private void ClkStartMoney()
        {
            if (LoadStatusPlayer.Status.CountMoney >= costRoulette)
            {
                LoadStatusPlayer.Status.CountMoney -= costRoulette;
                RouletteControl.SetThisDate();
                StartRoulette();
            }
        }

        private void ClkStartVideo()
        {
            if (Advertising.AdvertisingSee.SeeAdvertising())
            {
                StartRoulette();
            }
        }

        private void UpdateButton()
        {
            startMoveMoney.gameObject.SetActive(false);
            startMoveVideo.gameObject.SetActive(false);
            if (RouletteControl.IsActiveRoulette())
            {
                startMoveMoney.gameObject.SetActive(true);
            }
            else
            {
                startMoveVideo.gameObject.SetActive(true);
            }
        }

        private void StartRoulette()
        {
            startMoveMoney.interactable = false;
            startMoveVideo.interactable = false;
            close.interactable = false;
            StartCoroutine(startRouletteCur());
        }

        IEnumerator startRouletteCur()
        {

            float dist = Vector3.Distance(leftPoint.transform.position, items[numStopItem].position);
            while (dist > speedMove)
            {
                dist = Vector3.Distance(leftPoint.transform.position, items[numStopItem].position);

                leftPoint.padding.left -= (int)(Mathf.Min(dist, speedMove) * 100 * Time.deltaTime);
                leftPoint.SetLayoutHorizontal();
                yield return null;
            }

            yield return new WaitForSeconds(1f);
            pricesSpawn[numStopItem].OpenPanel.SetActive(true);
            pricesSpawn[numStopItem].PricePrefab.ToGivePrice();

            if (CallPlay != null)
            {
                CallPlay();
            }

            LoadStatusPlayer.SaveStatus();

            yield return new WaitForSeconds(2f);
            pricesSpawn[numStopItem].OpenPanel.SetActive(false);
            ResetRoulette();

            EndRoulette();
            yield break;
        }

        private void EndRoulette()
        {

            startMoveMoney.interactable = true;
            startMoveVideo.interactable = true;
            close.interactable = true;
            UpdateButton();
        }

        private void ResetRoulette()
        {
            ClearItems();
            for (int i = 0; i < countItem; i++)
            {
                Transform item = Instantiate(prefabItem, leftPoint.transform);
                items.Add(item);
                ItemPrice price = GetRandomPrice();
                pricesSpawn.Add(price);
                price.PricePrefab.ShowToUI(item);
            }
        }

        private void ClearItems()
        {
            for (int i = 0; i < items.Count; i++)
            {
                Destroy(items[i].gameObject);
            }
            items.Clear();
            pricesSpawn.Clear();
            leftPoint.padding.left = baseOffest;
        }

        private ItemPrice GetRandomPrice()
        {
            int coef = Random.Range(0, 100);
            for (int i = 0; i < prices.Length; i++)
            {
                if (prices[i].RandomValue >= coef)
                {
                    return prices[i];
                }
            }
            return prices[Random.Range(0, prices.Length)];
        }

        [System.Serializable]
        public class ItemPrice
        {
            public GameObject OpenPanel;

            public Price PricePrefab;

            public int RandomValue;
        }
    }
}