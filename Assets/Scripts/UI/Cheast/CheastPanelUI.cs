using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Base.Game;
using Base.Advertising;
using Base.Process;
using Base.Process.Price;

namespace Base.UI
{
    public class CheastPanelUI : PanelUI
    {
        [SerializeField] private MainMenu mainMenu;

        [SerializeField] private Transform contentKeys;

        [SerializeField] private GameObject keyPrefab;

        [SerializeField] private Button close;

        [SerializeField] private Button openCase;

        [SerializeField] private Button addKey;

        [SerializeField] private Button videoOpenCase;

        [SerializeField] private Transform contentCheastItem;

        [SerializeField] private CheastItemUI cheastItemPrefab;

        [Space(15)]
        [SerializeField] private ItemRandom[] normalPrices;

        [SerializeField] private ItemRandom[] videoPrices;


        private List<CheastItemUI> cheasts = new List<CheastItemUI>();

        private List<GameObject> keys = new List<GameObject>();

        private int countOpen;

        private bool isVideoOpen;

        private void Start()
        {
            GameProcess.CallEndGame += OpenFromGame;
            mainMenu.ChestsClk += OpenFromMenu;

            openCase.onClick.AddListener(delegate { OpenCheasts(); });
            addKey.onClick.AddListener(delegate { AddKey(); });
            videoOpenCase.onClick.AddListener(delegate { OpenCheastVideo(); });

            close.onClick.AddListener(delegate { mainMenu.ClosePanel(); });
        }

        private void OpenFromGame()
        {
            if (LoadStatusPlayer.Status.CountKey == 3)
            {
                mainMenu.OpenPanel(this);
                OpenCheasts();
            }
        }

        private void OpenFromMenu()
        {
            mainMenu.OpenPanel(this);
        }

        private void UpdateCountKeys()
        {
            for (int i = 0; i < keys.Count; i++)
            {
                Destroy(keys[i]);
            }
            keys.Clear();
            for (int i = 0; i < LoadStatusPlayer.Status.CountKey; i++)
            {
                GameObject key = Instantiate(keyPrefab, contentKeys);
                keys.Add(key);
            }
        }

        protected override void CallOpen()
        {

            UpdateCountKeys();
            UpdateCheast();
            UpdateButtons();
        }

        protected override void CallClose()
        {

        }

        private void UpdateCheast()
        {
            for (int i = 0; i < cheasts.Count; i++)
            {
                Destroy(cheasts[i].gameObject);
            }
            cheasts.Clear();

            for (int i = 0; i < 9; i++)
            {
                CheastItemUI cheast = Instantiate(cheastItemPrefab, contentCheastItem);
                cheast.Lock();
                cheasts.Add(cheast);
                cheast.InitCheast();
            }
        }

        private void UpdateButtons()
        {
            addKey.gameObject.SetActive(false);
            openCase.gameObject.SetActive(false);
            videoOpenCase.gameObject.SetActive(false);
            if (LoadStatusPlayer.Status.CountKey >= 3)
            {
                openCase.gameObject.SetActive(true);
            }
            else if (LoadStatusPlayer.Status.CountKey == 0)
            {
                videoOpenCase.gameObject.SetActive(true);
            }
            else
            {
                addKey.gameObject.SetActive(true);
            }
        }

        private void StartOpenCheast()
        {

            close.interactable = false;
            UpdateCountKeys();
            if (countOpen >= 6)
                UpdateCheast();
            addKey.interactable = false;
            openCase.interactable = false;
            videoOpenCase.interactable = false;

            for (int i = 0; i < cheasts.Count; i++)
            {
                cheasts[i].UnLock();
                cheasts[i].Clk += CheastClk;
            }
        }

        private void CheastClk(CheastItemUI cheast)
        {
            if (cheast.IsOpen) return;
            countOpen++;
            cheast.Open();
            Price price = GetRandomPrice();
            price.ToGivePrice();
            price.ShowToUI(cheast.Content);
            if (countOpen == 3 || countOpen >= 6)
            {
                EndOpenCheast();
            }
        }

        private void EndOpenCheast()
        {
            for (int i = 0; i < cheasts.Count; i++)
            {
                if (!cheasts[i].IsOpen)
                {
                    cheasts[i].Lock();
                }
            }

            close.interactable = true;
            UpdateButtons();
            addKey.interactable = true;
            openCase.interactable = true;
            videoOpenCase.interactable = true;
            LoadStatusPlayer.SaveStatus();
        }


        private void AddKey()
        {
            if (Advertising.AdvertisingSee.SeeAdvertising())
            {
                LoadStatusPlayer.Status.CountKey++;
                UpdateCountKeys();
                UpdateButtons();
                LoadStatusPlayer.SaveStatus();
            }
        }

        private void OpenCheasts()
        {
            isVideoOpen = true;
            LoadStatusPlayer.Status.CountKey -= 3;
            StartOpenCheast();
        }

        private void OpenCheastVideo()
        {
           if (isVideoOpen && Advertising.AdvertisingSee.SeeAdvertising())
            {
                isVideoOpen = false;
                StartOpenCheast();
            }
        }

        private Price GetRandomPrice()
        {
            int coef = Random.Range(0, 100);
            if (isVideoOpen)
            {
                for (int i = 0; i < videoPrices.Length; i++)
                {
                    if (videoPrices[i].RandomValue >= coef)
                    {
                        return videoPrices[i].PricePrefab;
                    }
                }
                return videoPrices[Random.Range(0, videoPrices.Length)].PricePrefab;
            }
            else
            {
                for (int i = 0; i < normalPrices.Length; i++)
                {
                    if (normalPrices[i].RandomValue >= coef)
                    {
                        return normalPrices[i].PricePrefab;
                    }
                }
                return normalPrices[Random.Range(0, normalPrices.Length)].PricePrefab;
            }
        }

        [System.Serializable]
        public class ItemRandom
        {
            [Range(0, 100)]
            public int RandomValue;
            public Price PricePrefab;
        }
    }
}