using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


namespace Base.UI
{
    public delegate void ClkCheast(CheastItemUI cheast);

    public class CheastItemUI : MonoBehaviour
    {
        public event ClkCheast Clk;

        public bool IsOpen { get; private set; }

        public Transform Content
        {
            get
            {
                return content;
            }
        }

        [SerializeField] private Button decter;

        [SerializeField] private Image imgCheast;

        [SerializeField] private Transform content;


        public void InitCheast()
        {
            decter.onClick.AddListener(delegate { if (Clk != null) Clk(this); });
        }

        public void Open()
        {
            imgCheast.gameObject.SetActive(false);
            IsOpen = true;
        }

        public void Lock()
        {
            decter.interactable = false;
        }

        public void UnLock()
        {
            decter.interactable = true;
        }

    }
}