using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

namespace Base.UI
{
    public class MainMenu : MonoBehaviour
    {
        public event Action StartGameClk;

        public event Action TaskGamePanelClk;

        public event Action ShopCarsClk;

        public event Action ChestsClk;

        public event Action RouletteClk;

        public event Action OptionClk;

        public event Action DailyBonusClk;

        [SerializeField] private Button startGame;
        [SerializeField] private Button tasksGame;
        [SerializeField] private Button shopCars;
        [SerializeField] private Button chests;
        [SerializeField] private Button roulette;
        [SerializeField] private Button option;
        [SerializeField] private Button dailyBonus;

        [Space(15)]
        [Header("Move panels")]
        [SerializeField] private MovePanelUI upPanel;
        [SerializeField] private MovePanelUI leftPanel;
        [SerializeField] private MovePanelUI rightPanel;
        [SerializeField] private MovePanelUI downPanel;
        [SerializeField] private GameObject blackoutPanel;

        [Space(15)]
        [SerializeField] private GameUI gameUI;

        private bool isOpen;

        private PanelUI openPanel;

        private void Awake()
        {
            isOpen = true;
            startGame.onClick.AddListener(delegate { if(openPanel != null) return; if (StartGameClk != null) StartGameClk(); });
            tasksGame.onClick.AddListener(delegate { if (TaskGamePanelClk != null) TaskGamePanelClk(); });
            shopCars.onClick.AddListener(delegate { if (ShopCarsClk != null) ShopCarsClk(); });
            chests.onClick.AddListener(delegate { if (ChestsClk != null) ChestsClk(); });
            roulette.onClick.AddListener(delegate { if (RouletteClk != null) RouletteClk(); });
            option.onClick.AddListener(delegate { if (OptionClk != null) OptionClk(); });
            dailyBonus.onClick.AddListener(delegate { if (DailyBonusClk != null) DailyBonusClk(); });
        }

        public void StartProcess()
        {
            
        }

        public void CloseMenu()
        {
            if (isOpen)
            {
                isOpen = false;
                upPanel.MoveOff();
                leftPanel.MoveOff();
                rightPanel.MoveOff();
                downPanel.MoveOff();

                gameUI.UIOn();
            }
        }

        public void OpenMenu()
        {
            if (!isOpen)
            {
                isOpen = true;
                upPanel.MoveOn();
                leftPanel.MoveOn();
                rightPanel.MoveOn();
                downPanel.MoveOn();

                gameUI.UIOff();
            }
        }

        public void OpenPanel(PanelUI panel) {
            if (openPanel == null) {
                openPanel = panel;
                panel.Open();
                blackoutPanel.SetActive(true);
            }
        }

        public void ClosePanel() {
            if (openPanel != null)
            {
                openPanel.Close();
                openPanel = null;
                blackoutPanel.SetActive(false);
            }
        }
    }
}