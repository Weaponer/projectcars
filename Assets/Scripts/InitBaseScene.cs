using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Base.Process;

namespace Base.InitGame
{

    public class InitBaseScene : MonoBehaviour
    {
        [SerializeField] private Game.GameProcess gameProcess;

        [SerializeField] private TasksControl tasksControl;

        [SerializeField] private DailyBonusControl dailyBonusControl;

        [SerializeField] private Vibration vibration;

        [SerializeField] private RouletteControl rouletteControl;

        private void Awake()
        {
            LoadStatusPlayer.LoadStatus();
            dailyBonusControl.InitDailuBonus();
        }

        private void Start()
        {
            tasksControl.InitTasks();
            rouletteControl.InitRoulette();
            gameProcess.InitGame();
            vibration.InitVibration();
        }
    }
}