using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;

namespace Base.Process
{
    public static class LoadStatusPlayer
    {
        public static StatusFile Status { get; private set; }


#if UNITY_EDITOR
        private static string path = Application.dataPath;
#else
        private static string path = Application.persistentDataPath;
#endif
        private static string name = "sf.sf";

        public static void LoadStatus() {
            if (!File.Exists(path + "/" + name)) {
                Status = StatusFile.Default();
                SaveStatus();
                return;
            }
            using (FileStream file = File.Open(path + "/" + name, FileMode.Open)) {
                BinaryFormatter bf = new BinaryFormatter();
                Status = (StatusFile)bf.Deserialize(file);
                file.Close();
            }
        }

        public static void SaveStatus()
        {
            using (FileStream file = File.Open(path + "/" + name, FileMode.OpenOrCreate)) {
                BinaryFormatter bf = new BinaryFormatter();
                bf.Serialize(file, Status);
                file.Close();
            }
        }
    }
}