using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

namespace Base.Process
{
    public class TasksControl : MonoBehaviour
    {
        public static TaskObject[] TaskListData { get; private set; }

        [SerializeField] private TaskDataBase taskData;

        [SerializeField] private int countTaskEasy;

        [SerializeField] private int countTaskHard;



        public void InitTasks()
        {
            TaskList task = LoadStatusPlayer.Status.Tasks;
            if ((task.Tasks == null || task.Tasks.Length == 0) || CheckIsOldTime())
            {
                GenerateTasks();
            }
            else
            {
                StartTasks();
            }
            StartInvoke();
        }


        private void StartTasks()
        {
            Debug.Log("Start task");
            TaskList task = LoadStatusPlayer.Status.Tasks;
            TaskListData = new TaskObject[task.Tasks.Length];
            for (int i = 0; i < task.Tasks.Length; i++)
            {
                TaskListData[i] = taskData.GetTastWithData(task.Tasks[i]);
                TaskListData[i].StartTask(task.Tasks[i]);
            }
        }

        private bool CheckIsOldTime()
        {
            Debug.Log("Check time");
            DateTime time = DateTime.Now;
            TaskList task = LoadStatusPlayer.Status.Tasks;
            if (task.Day != time.Day || task.Month != time.Month || task.Year != time.Year)
            {
                return true;
            }
            return false;
        }

        private void GenerateTasks()
        {
            Debug.Log("Create task");
            TaskList task = LoadStatusPlayer.Status.Tasks;
            task.Tasks = new TaskData[countTaskHard + countTaskEasy];
            TaskObject[] tasks = taskData.GetRandomTask(countTaskEasy, countTaskHard);
            TaskListData = tasks;
            for (int i = 0; i < task.Tasks.Length; i++)
            {
                task.Tasks[i] = TaskListData[i].GenerateData();
                tasks[i].StartTask(task.Tasks[i]);
            }

            DateTime time = DateTime.Now;
            task.Day = time.Day;
            task.Month = time.Month;
            task.Year = time.Year;
        }

        private void GenerateNewTasksTime()
        {
            GenerateTasks();
            StartInvoke();
        }

        private void StartInvoke()
        {

            DateTime time = DateTime.Now;
            DateTime nextDay = time.AddDays(1).Date;
            double seconds = (nextDay - time).TotalSeconds;
            // Debug.Log("Start invoke " + seconds);
            //Invoke("GenerateNewTasksTime", (float)seconds);*/
        }
    }
}