using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

namespace Base.Process
{
    public class DailyBonusControl : MonoBehaviour
    {
        [SerializeField] private DailyPriceDataBase dailyPriceData;
        public void InitDailuBonus()
        {
            if (LoadStatusPlayer.Status.Dailu.Year == 0)
            {
                GenerateBonuse();
            }
            else if (CheckTime())
            {
                GenerateBonuse();
            }
        }

        private bool CheckTime()
        {
            DateTime timeOld = new DateTime(LoadStatusPlayer.Status.Dailu.Year, LoadStatusPlayer.Status.Dailu.Month, LoadStatusPlayer.Status.Dailu.Day);
            DateTime timeCur = DateTime.Now;
            timeCur = new DateTime(timeCur.Year, timeCur.Month, timeCur.Day);
            if ((timeCur - timeOld).Days >= 7)
            {
                return true;
            }
            return false;
        }

        private void GenerateBonuse()
        {
            LoadStatusPlayer.Status.Dailu.CounOpen = 0;
            DateTime time = DateTime.Now;
            LoadStatusPlayer.Status.Dailu.Day = time.Day;
            LoadStatusPlayer.Status.Dailu.Month = time.Month;
            LoadStatusPlayer.Status.Dailu.Year = time.Year;
            LoadStatusPlayer.Status.Dailu.listPrice = new int[7];
            for (int i = 0; i < 7; i++)
            {
                LoadStatusPlayer.Status.Dailu.listPrice[i] = UnityEngine.Random.Range(0, dailyPriceData.Count);
            }
        }
    }
}