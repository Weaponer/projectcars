using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

namespace Base.Process
{
    public class RouletteControl : MonoBehaviour
    {

        private static bool isActive;
        public void InitRoulette()
        {
            DateTime cur = DateTime.Now;
            RouletteList rouletteList = LoadStatusPlayer.Status.Roulette;
            if (rouletteList.Year == 0)
            {
                isActive = true;
                rouletteList.Year = cur.Year;
                rouletteList.Month = cur.Month;
                rouletteList.Day = cur.Day;
            }
            else if (rouletteList.Year != cur.Year || rouletteList.Month != cur.Month || rouletteList.Day != cur.Day)
            {
                isActive = true;
            }
            else
            {
                isActive = false;
            }
            LoadStatusPlayer.SaveStatus();
        }

        public static void SetThisDate()
        {
            isActive = false;
            RouletteList rouletteList = LoadStatusPlayer.Status.Roulette;
            DateTime cur = DateTime.Now;
            rouletteList.Year = cur.Year;
            rouletteList.Month = cur.Month;
            rouletteList.Day = cur.Day;
            LoadStatusPlayer.SaveStatus();
        }

        public static bool IsActiveRoulette()
        {
            return isActive;
        }
    }
}