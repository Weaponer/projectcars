using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Base.Process
{
    public class QuitGame : MonoBehaviour
    {
        private void OnApplicationQuit()
        {
            LoadStatusPlayer.SaveStatus();
        }
    }
}