using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

namespace Base.Process
{
    [Serializable]
    public class StatusFile
    {
        public bool TutorialEnd;
        public List<int> NumOpenCars;
        public int TakeCar;

        public bool IsSetFiveRate;

        public int CountKey;
        public int CountMoney;
        public int MaxScore;

        public bool Music;
        public bool Sound;
        public bool Vibration;

        public int shopIndexMultiply;

        public TaskList Tasks;

        public DailuBonusList Dailu;

        public RouletteList Roulette;

        public void AddCar(int index)
        {
            for (int i = 0; i < NumOpenCars.Count; i++)
            {
                if (NumOpenCars[i] == index)
                {
                    return;
                }
            }
            NumOpenCars.Add(index);
        }

        public static StatusFile Default()
        {
            StatusFile statusFile = new StatusFile();
            statusFile.TutorialEnd = false;
            statusFile.TakeCar = 0;
            statusFile.NumOpenCars = new List<int>();
            statusFile.NumOpenCars.Add(0);
            statusFile.CountMoney = 10000;
            statusFile.CountKey = 0;
            statusFile.MaxScore = 0;
            statusFile.Music = true;
            statusFile.Sound = true;
            statusFile.Vibration = true;
            statusFile.IsSetFiveRate = false;
            statusFile.shopIndexMultiply = 1;

            statusFile.Tasks = new TaskList();
            statusFile.Dailu = new DailuBonusList();
            statusFile.Roulette = new RouletteList();


            return statusFile;
        }
    }

    [Serializable]
    public class TaskList
    {
        public int Year;
        public int Month;
        public int Day;

        public TaskData[] Tasks;
    }

    [Serializable]
    public class DailuBonusList
    {
        public int Year;
        public int Month;
        public int Day;

        public int CounOpen;

        public int[] listPrice;
    }

    [Serializable]
    public class RouletteList
    {
        public int Year;
        public int Month;
        public int Day;

        public bool IsActive;
    }
}